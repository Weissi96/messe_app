// Sending and receiving data in JSON format using POST method
//
var http = require('http');
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var xhr = new XMLHttpRequest();
var url = "http://localhost:3000/api/beratung";
xhr.open("POST", url, true);
xhr.setRequestHeader("Content-Type", "application/json");
xhr.onreadystatechange = function () {
    if (xhr.readyState === 4 && xhr.status === 200) {
        var json = JSON.parse(xhr.responseText);
        console.log(json.email + ", " + json.password);
    }
};
var data = JSON.stringify({"veranstaltungId": 2, "beratungen": [{ "session": [{"von": "1"},{"von": "2"}] }, { "session": [{"von": "3"},{"von": "4"}] }]});
xhr.send(data);