let mysql = require('mysql');

function connectDB(){
  connection.connect(function(err) {});
}

function executeSQL(query, callback){
  var connection = mysql.createConnection({
    host: 'e113900-mysql.services.easyname.eu',
    user: 'u177115db1',
    password: '.6b9xp6aenui',
    database: 'u177115db1'
  });
    connection.query(query, function (err, result, fields) {
      if (err) throw err;
      
      connection.end(function(err) {
        if (err) {
          return console.log('error:' + err.message);
        }
      });
      callback(null, result);
      connection.end(function(err) {
        if (err) {
          return console.log('error:' + err.message);
        }
        console.log('Close the database connection.');
      });
    });
}

module.exports = {
    connectDB: function () {
      connectDB();
    },
    // auth
    loginUser: function (email, pass, callback) {
        executeSQL("SELECT * FROM tblBerater WHERE Email='"+email+"' and Passwort = '"+pass+"'", callback);
    },

    // veranstaltung
    createVeranstaltung: function (name, von, bis, kosten, veranstalterId, standortId, sujetId, briefingId, callback) {
        executeSQL("INSERT INTO `tblVeranstaltung` (`VeranstaltungId`, `Veranstaltung`, `Von`, `Bis`, `Kosten`, `VeranstalterId`, `StandortId`, `SujetId`, `BriefingId`, `Aktiv`) VALUES (NULL, '"+ name +"', '"+ von +"', '"+ bis +"', '"+ kosten +"', '"+ veranstalterId +"', '"+ standortId +"', '"+ sujetId +"', '"+ briefingId +"', '1');", callback);
    },
    getVeranstaltungById: function (veranstaltungId, callback) {
      executeSQL("SELECT * FROM tblVeranstaltung WHERE VeranstaltungId = " + veranstaltungId, callback);
    },
    updateVeranstaltung: function (veranstaltungId, name, von, bis, kosten, veranstalterId, standortId, sujetId, briefingId, callback) {
      executeSQL("UPDATE tblVeranstaltung SET Veranstaltung = '" + name +"', Von = '" + von +"', Bis = '" + bis +"', Kosten = '" + kosten +"', VeranstalterId = '" + veranstalterId +"', StandortId = '" + standortId +"', SujetId = '" + sujetId +"', BriefingId = '" + briefingId +"' WHERE VeranstaltungId = " + veranstaltungId, callback);
    },
    getUpcomingVeranstaltungen: function (callback) {
        executeSQL("SELECT VeranstaltungId, Veranstaltung FROM tblVeranstaltung WHERE Aktiv = 1", callback);
    },
    deleteVeranstaltung: function (veranstaltungsId, callback) {
      executeSQL("UPDATE tblVeranstaltung SET Aktiv = 0 WHERE VeranstaltungId = " + veranstaltungsId, callback);
    },

    // veranstalter
    createVeranstalter: function (veranstalter, schuleId, callback) {
      executeSQL("INSERT INTO `tblVeranstalter`(`VeranstalterId`, `Veranstalter`, `SchuleId`, `Aktiv`) VALUES (NULL, '"+ veranstalter +"', '"+ schuleId +"','1');", callback);
    },
    getVeranstalterById: function (veranstalterId, callback) {
      executeSQL("SELECT * FROM tblVeranstalter WHERE VeranstalterId = " + veranstalterId, callback);
    },
    updateVeranstalter: function (veranstalterId, veranstalter, schuleId, callback) {
      executeSQL("UPDATE tblVeranstalter SET Veranstalter = '" + veranstalter +"', SchuleId = '" + schuleId +"' WHERE VeranstalterId = " + veranstalterId, callback);
    },
    getVeranstalter: function (callback) {
        executeSQL("SELECT * FROM tblVeranstalter WHERE Aktiv = 1", callback);
    },
    deleteVeranstalter: function (veranstalterId, callback) {
      executeSQL("UPDATE tblVeranstalter SET Aktiv = 0 WHERE VeranstalterId = " + veranstalterId, callback);
    },

    // abschluss
    getAbschluss: function (callback) {
        executeSQL("SELECT * FROM tblAbschluss WHERE Aktiv = 1", callback);
    },

    // campus
    createCampus: function (campus, beschreibung, callback) {
      executeSQL("INSERT INTO `tblCampus`(`CampusId`, `Campus`, `Beschreibung`, `Aktiv`) VALUES (NULL, '"+ campus +"', '"+ beschreibung +"','1');", callback);
    },
    getCampusById: function (campusId, callback) {
      executeSQL("SELECT * FROM tblCampus WHERE CampusId = " + campusId, callback);
    },
    updateCampus: function (campusId, campus, beschreibung, callback) {
      executeSQL("UPDATE tblCampus SET Campus = '" + campus +"', Beschreibung = '" + beschreibung +"' WHERE CampusId = " + campusId, callback);
    },
    getCampus: function (callback) {
        executeSQL("SELECT * FROM tblCampus WHERE Aktiv = 1", callback);
    },
    deleteCampus: function (campusId, callback) {
      executeSQL("UPDATE tblCampus SET Aktiv = 0 WHERE CampusId = " + campusId, callback);
    },

    // studiengang
    createStudiengang: function (kürzel, studiengang, beschreibung, abschluss, campus, callback) {
      executeSQL("INSERT INTO `tblStudiengang`(`StudiengangId`, `Kürzel`, `Studiengang`, `Beschreibung`, `CampusId`, `AbschlussId`, `Aktiv`) VALUES (NULL, '"+ kürzel +"', '"+ studiengang +"', '"+ beschreibung +"','"+ campus +"','"+ abschluss +"','1');", callback);
    },
    getStudiengangById: function (studiengangId, callback) {
      executeSQL("SELECT * FROM tblStudiengang WHERE StudiengangId = " + studiengangId, callback);
    },
    updateStudiengang: function (studiengangId, kürzel, studiengang, beschreibung, abschluss, campus, callback) {
      executeSQL("UPDATE tblStudiengang SET Studiengang = '" + studiengang +"', Beschreibung = '" + beschreibung +"', Kürzel = '" + kürzel +"', AbschlussId = '" + abschluss +"', CampusId = '" + campus +"' WHERE StudiengangId = " + studiengangId, callback);
    },
    getStudiengang: function (callback) {
        executeSQL("SELECT * FROM tblStudiengang WHERE Aktiv = 1", callback);
    },
    getStudiengangByCampus: function (campusId, callback) {
      executeSQL("SELECT * FROM tblStudiengang AS S INNER JOIN tblAbschluss AS A ON S.AbschlussId = A.AbschlussId WHERE S.Aktiv = 1 AND CampusId = " + campusId, callback);
    },
    deleteStudiengang: function (campusId, callback) {
      executeSQL("UPDATE tblStudiengang SET Aktiv = 0 WHERE StudiengangId = " + campusId, callback);
    },

    // sujet
    createSujet: function (sujet, callback) {
      executeSQL("INSERT INTO `tblSujet`(`SujetId`, `Sujet`, `Aktiv`) VALUES (NULL, '"+ sujet +"','1');", callback);
    },
    getSujetById: function (sujetId, callback) {
      executeSQL("SELECT * FROM tblSujet WHERE SujetId = " + sujetId, callback);
    },
    updateSujet: function (sujetId, sujet, callback) {
      executeSQL("UPDATE tblSujet SET Sujet = '" + sujet +"' WHERE SujetId = " + sujetId, callback);
    },
    getSujet: function (callback) {
        executeSQL("SELECT * FROM tblSujet WHERE Aktiv = 1", callback);
    },
    deleteSujet: function (sujetId, callback) {
      executeSQL("UPDATE tblSujet SET Aktiv = 0 WHERE SujetId = " + sujetId, callback);
    },

    // Berater
    createBerater: function (vorname, nachname, email, passwort, matrikelnr, beratertyp, campusId, studiengangId, callback) {
      executeSQL("INSERT INTO `tblBerater` (`BeraterId`, `Vorname`, `Nachname`, `EMail`, `Passwort`, `CampusId`, `StudiengangId`, `MatrikelNr`, `BeratertypId`, `Aktiv`) VALUES (NULL, '" + vorname + "', '" + nachname + "', '" + email + "', '" + passwort + "', '" + campusId + "', '" + studiengangId + "', '" + matrikelnr + "', '" + beratertyp + "', '1');", callback);
    },
    getBerater: function (callback) {
      executeSQL("SELECT BeraterId, Vorname, Nachname FROM tblBerater WHERE Aktiv = '1'", callback);
    },
    updateBerater: function (beraterId, vorname, nachname, email, passwort, matrikelnr, beratertyp, campusId, studiengangId, callback) {
      executeSQL("UPDATE tblBerater SET Vorname = '" + vorname +"', Nachname = '" + nachname +"', EMail = '" + email +"', Passwort = '" + passwort +"', MatrikelNr = '" + matrikelnr +"', BeraterTypId = '" + beratertyp +"', CampusId = '" + campusId +"', StudiengangId = '" + studiengangId +"' WHERE BeraterId = " + beraterId, callback);
    },
    deleteBerater: function (beraterId, callback) {
      executeSQL("UPDATE tblBerater SET Aktiv = 0 WHERE BeraterId = " + beraterId, callback);
    },
    getBeraterById: function (beraterId, callback) {
      executeSQL("SELECT * FROM tblBerater WHERE BeraterId = " + beraterId, callback);
    },
    //Interesse
    createInteresse: function (fachgebiet, callback) {
      executeSQL("INSERT INTO `tblFachgebiet`(`FachgebietId`, `Fachgebiet`, `Aktiv`) VALUES (NULL,'" + fachgebiet + "','1');", callback);
    },
    getInteressen: function (callback) {
      executeSQL("SELECT `FachgebietId`, `Fachgebiet` FROM `tblFachgebiet` WHERE Aktiv = '1'", callback);
    },
    updateInteresse: function (fachgebietId, fachgebiet, callback) {
      executeSQL("UPDATE tblFachgebiet SET Fachgebiet='" + fachgebiet + "' WHERE FachgebietId = " + fachgebietId, callback);
    },
    deleteInteresse: function (fachgebietId, callback) {
      executeSQL("UPDATE tblFachgebiet SET Aktiv = 0 WHERE FachgebietId = " + fachgebietId, callback);
    },
    getInteresseById: function (interesseId, callback) {
      executeSQL("SELECT * FROM tblFachgebiet WHERE FachgebietId = " + interesseId, callback);
    },

    //Angebote
    createAngebot: function (angebot, beschreibung, callback) {
      executeSQL("INSERT INTO `tblAngebot`(`AngebotId`, `Angebot`, `Beschreibung`, `Aktiv`) VALUES (NULL,'" + angebot + "','" + beschreibung + "','1')", callback);
    },
    getAngebote: function (callback) {
      executeSQL("SELECT `AngebotId`, `Angebot` FROM `tblAngebot` WHERE Aktiv = '1'", callback);
    },
    updateAngebot: function (angebotId, angebot, beschreibung, callback) {
      executeSQL("UPDATE tblAngebot SET Angebot='" + angebot + "' ,Beschreibung='" + beschreibung + "' WHERE AngebotId = " + angebotId, callback);
    },
    deleteAngebot: function (angebotId, callback) {
      executeSQL("UPDATE tblAngebot SET Aktiv = 0 WHERE AngebotId = " + angebotId, callback);
    },
    getAngbebotById: function (angebotId, callback) {
      executeSQL("SELECT * FROM tblAngebot WHERE AngebotId = " + angebotId, callback);
    },

    //Schule
    createSchule: function (schule, ausbildungId, callback) {
      executeSQL("INSERT INTO `tblSchule`(`SchuleId`, `Schule`, `AusbildungId`, `Aktiv`) VALUES (NULL,'" + schule + "','" + ausbildungId + "','1')", callback);
    },
    getSchulen: function (callback) {
      executeSQL("SELECT `SchuleId`, `Schule` FROM `tblSchule` WHERE Aktiv = '1'", callback);
    },
    updateSchule: function (schuleId, schule, ausbildungId, callback) {
      executeSQL("UPDATE tblSchule SET Schule='" + schule + "' ,AusbildungId='" + ausbildungId + "' WHERE SchuleId = " + schuleId, callback);
    },
    deleteSchule: function (schuleId, callback) {
      executeSQL("UPDATE tblSchule SET Aktiv = 0 WHERE SchuleId = " + schuleId, callback);
    },
    getSchuleById: function (schuleId, callback) {
      executeSQL("SELECT * FROM tblSchule WHERE SchuleId = " + schuleId, callback);
    },

    //Gewinnspiel
    getGewinnspiel: function (callback) {
      executeSQL("SELECT * FROM `tblGewinnspiel` WHERE Aktiv = '1'", callback);
    },
    updateGewinnspiel: function (gewinnspielId, gewinnspiel, beschreibung, callback) {
      executeSQL("UPDATE tblGewinnspiel SET Gewinnspiel='" + gewinnspiel + "' ,Beschreibung='" + beschreibung + "' WHERE GewinnspielId = " + gewinnspielId, callback);
    },
    deleteGewinnspiel: function (gewinnspielId, callback) {
      executeSQL("UPDATE tblGewinnspiel SET Aktiv = 0 WHERE GewinnspielId = " + gewinnspielId, callback);
    },
    getGewinnspielById: function (gewinnspielId, callback) {
      executeSQL("SELECT * FROM tblGewinnspiel WHERE GewinnspielId = " + gewinnspielId, callback);
    },

    // Preis
    createPreis: function (preis, beschreibung, gewinnspielId, callback) {
      executeSQL("INSERT INTO `tblPreis`(`PreisId`, `Preis`, `Beschreibung`, `GewinnspielId`, `Aktiv`)  VALUES (NULL,'" + preis + "','" + beschreibung + "','" + gewinnspielId + "','1')", callback);
    },

    //Ausbildung
    createAusbildung: function (ausbildung, callback) {
      executeSQL("INSERT INTO `tblAusbildung`(`AusbildungId`, `Ausbildung`, `Aktiv`)   VALUES (NULL,'" + ausbildung + "','1')", callback);
    },
    getAusbildung: function (callback) {
      executeSQL("SELECT * FROM `tblAusbildung` WHERE Aktiv = '1'", callback);
    },
    updateAusbildung: function (ausbildungId, ausbildung, callback) {
      executeSQL("UPDATE tblAusbildung SET Ausbildung='" + ausbildung + "' WHERE AusbildungId = " + ausbildungId, callback);
    },
    deleteAusbildung: function (ausbildungId, callback) {
      executeSQL("UPDATE tblAusbildung SET Aktiv = 0 WHERE AusbildungId = " + ausbildungId, callback);
    },
    getAusbildungById: function (ausbildungId, callback) {
      executeSQL("SELECT * FROM tblAusbildung WHERE AusbildungId = " + ausbildungId, callback);
    },

    // termine
    getTermineById: function (beraterId, callback) {
      executeSQL("SELECT * FROM tblTermine WHERE BeraterId = " + beraterId, callback);
    },

    // SQL Routines
    fillCreateVeranstaltung: function (callback) {
      executeSQL("CALL `fillCreateVeranstaltung`();", callback);
    },
    fillCreateStudiengang: function (callback) {
      executeSQL("CALL `fillCreateStudiengang`();", callback);
    },
    fillCreateSchule: function (callback) {
      executeSQL("CALL `fillCreateSchule`();", callback);
    },
    fillCreateVeranstalter: function (callback) {
      executeSQL("CALL `fillCreateVeranstalter`();", callback);
    },
    fillVeranstaltung: function (veranstaltungId, callback) {
      executeSQL("CALL `fillVeranstaltung`(" + veranstaltungId + ");", callback);
    },
    fillBeratung: function (beratungId, callback) {
      executeSQL("CALL `fillBeratung`(" + beratungId + ");", callback);
    },
    fillBerater: function (beraterId, callback) {
      executeSQL("CALL `fillBerater`(" + beraterId + ");", callback);
    },
    createGewinnspiel: function (gewinnspiel, beschreibung, callback) {
      executeSQL("CALL `createGewinnspiel`('" + gewinnspiel + "','" + beschreibung + "');", callback);
    },
    createBeratung: function (veranstaltungsId, callback) {
      executeSQL("CALL `createBeratung`('" + veranstaltungsId + "');", callback);
    },
    fillCreateBerater: function (callback) {
      executeSQL("CALL `fillCreateBerater`();", callback);
    },
    createSession: function (von,bis,bewertung,interessentId,beraterId,beratungId, callback) {
      executeSQL("CALL `createSession`('" + von + "','" + bis + "','" + bewertung + "','" + interessentId + "','" + beraterId + "','" + beratungId + "');", callback);
    },
    loginByPassword: function (password, callback) {
      executeSQL("CALL `loginByPassword`('" + password + "');", callback);
    },
    addBeraterByVeranstaltungsId: function (beraterId, veranstaltungId, password, callback) {
      executeSQL("CALL `addBeraterByVeranstaltungsId`('" + beraterId + "','" + veranstaltungId + "','" + password + "');", callback);
    },
    removeBeraterByVeranstaltungsId: function (beraterId, veranstaltungId, callback) {
      executeSQL("CALL `removeBeraterByVeranstaltungsId`('" + beraterId + "','" + veranstaltungId + "');", callback);
    },
    getOneStopFlyerInfoById: function (id, callback) {
      executeSQL("CALL `getOneStopFlyerInfoById`('" + id + "');", callback);
    }
};