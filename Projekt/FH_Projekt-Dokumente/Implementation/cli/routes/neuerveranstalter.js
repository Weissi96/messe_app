const express = require('express');
const router = express.Router();
var common = require('./common');
var database = require('../database/database');

// translation
var renderName = 'neuerveranstalter';
var redirectRenderName = 'veranstalter';
var titleEdit = 'Veranstalter bearbeiten';
var titleAdd = 'Veranstalter hinzufügen';

router.get('/', (req, res) => {
  var query_id = req.query.id;
  if(query_id){
    database.getVeranstalterById(query_id, function(error, result) {
      var veranstalter = { veranstalterId:result[0].VeranstalterId, name:result[0].Veranstalter, schuleId: result[0].SchuleId};
      init(veranstalter, titleEdit, req, res);
    });
  } else {
    init(null, titleAdd, req, res);
  }
});

function init(veranstalter, title, req, res) {
  database.fillCreateVeranstalter(function(error, result) {
    var schule = {};
    Object.keys(result).forEach(function(key) {
      switch (key) {
        case '0':
          Object.keys(result[key]).forEach(function(keySchule) {
            var row = result[key][keySchule];
            schule[keySchule] = { name: row.Schule, id: row.SchuleId };
          });
          break;
      }
    });
    return res.render(renderName, { title: title, user: common.getUser(req), veranstalter: veranstalter, schule: schule });
  });
}

router.post('/', (req, res) => {
  var post  = req.body;
  var name = post.name;
  var schule = post.schuleId;
  var id = post.id;
  if(post.submit == titleEdit){
    database.updateVeranstalter(id,name, schule, function(error, result) {
      res.redirect(redirectRenderName);
    });
  } else {
    database.createVeranstalter(name, schule, function(error, result) {
      res.redirect(redirectRenderName);
    });
  }
});

module.exports = router;