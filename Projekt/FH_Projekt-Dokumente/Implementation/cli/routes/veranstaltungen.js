const express = require('express');
const router = express.Router();
const database = require('../database/database');
var common = require('../routes/common');

// translation
var renderName = 'veranstaltungen';
var title = 'Veranstaltungen';
var buttonEdit = 'Bearbeiten';
var successDelete = 'Veranstaltung erfolgreich gelöscht';

router.get('/', (req, res) => {
  init(null, req, res);
});

router.post('/', (req, res) => {
  var post  = req.body;
  var id = post.id;
  if(post.submit == buttonEdit){
    res.redirect('/neueveranstaltung?id=' + id);
  } else {
    database.deleteVeranstaltung(id,function(error, result) {
        var keyVal = { successDelete };
        init(keyVal, req, res);
    });
  }
});

function init(messages, req, res){
  database.getUpcomingVeranstaltungen(function(error, result) {
    var keyVal = {};
    Object.keys(result).forEach(function(key) {
      var row = result[key];
      keyVal[row.VeranstaltungId] = row.Veranstaltung;
    });
    res.render(renderName, { title: title, user: common.getUser(req), veranstaltungen: keyVal, successmessages: messages });
  });
}

module.exports = router;