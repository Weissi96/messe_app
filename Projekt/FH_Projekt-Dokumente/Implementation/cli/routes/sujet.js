const express = require('express');
const router = express.Router();
const database = require('../database/database');
var common = require('./common');

// translation
var renderName = 'sujet';
var title = 'Sujet';
var successDelete = 'Sujet erfolgreich gelöscht';
var titleButtonEdit = 'Bearbeiten';

router.get('/', (req, res) => {
  init(null, req, res);
});

router.post('/', (req, res) => {
  var post  = req.body;
  var id = post.id;
  if(post.submit == titleButtonEdit){
    res.redirect('/neuessujet?id=' + id);
  } else {
    database.deleteSujet(id,function(error, result) {
        var keyVal = { successDelete };
        init(keyVal, req, res);
    });
  }
});

function init(messages, req, res){
  database.getSujet(function(error, result) {
    var keyVal = {};
    Object.keys(result).forEach(function(key) {
      var row = result[key];
      keyVal[row.SujetId] = row.Sujet;
    });
    res.render(renderName, { title: title, user: common.getUser(req), sujet: keyVal, successmessages: messages });
  });
}

module.exports = router;