const express = require('express');
const router = express.Router();
const database = require('../database/database');
var common = require('../routes/common');

// translation
var renderName = 'studiengang';
var title = 'Studiengang';
var successDelete = 'Studiengang erfolgreich gelöscht';
var titleButtonEdit = 'Bearbeiten';

router.get('/', (req, res) => {
  init(null, req, res);
});

router.post('/', (req, res) => {
  var post  = req.body;
  var id = post.id;
  if(post.submit == titleButtonEdit){
    res.redirect('/neuerstudiengang?id=' + id);
  } else {
    database.deleteStudiengang(id,function(error, result) {
        var keyVal = { successDelete };
        init(keyVal, req, res);
    });
  }
});

function init(messages, req, res){
  database.getStudiengang(function(error, result) {
    var keyVal = {};
    Object.keys(result).forEach(function(key) {
      var row = result[key];
      keyVal[row.StudiengangId] = row.Studiengang;
    });
    res.render(renderName, { title: title, user: common.getUser(req), studiengang: keyVal, successmessages: messages });
  });
}

module.exports = router;