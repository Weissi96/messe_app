const express = require('express');
const router = express.Router();
var common = require('./common');
var database = require('../database/database');

// translation
var renderName = 'neueschule';
var redirectRenderName = 'schule';
var titleEdit = 'Schule bearbeiten';
var titleAdd = 'Schule hinzufügen';

router.get('/', (req, res) => {
  var query_id = req.query.id;
  if(query_id){
    database.getSchuleById(query_id, function(error, result) {
      var schule = { schuleId:result[0].SchuleId, schule:result[0].Schule, ausbildungId:result[0].AusbildungId};
      init(schule, titleEdit, req, res);
    });
  } else {
    init(null, titleAdd, req, res);
  }
});


function init(schule, title, req, res) {
  database.fillCreateSchule(function(error, result) {
    var ausbildung = {};
    Object.keys(result).forEach(function(key) {
      switch (key) {
        case '0':
          Object.keys(result[key]).forEach(function(keyAusbildung) {
            var row = result[key][keyAusbildung];
            ausbildung[keyAusbildung] = { name: row.Ausbildung, id: row.AusbildungId };
          });
          break;
      }
    });
    return res.render(renderName, { title: title, user: common.getUser(req), schule: schule, ausbildung: ausbildung });
  });
}

router.post('/', (req, res) => {
  var post  = req.body;
  console.log(post);
  var schule = post.schule;
  var abschluss = post.ausbildungId;
  var id = post.id;
  if(post.submit == titleEdit){
    database.updateSchule(id, schule, abschluss, function(error, result) {
      res.redirect(redirectRenderName);
    });
  } else {
    database.createSchule(schule, abschluss, function(error, result) {
      res.redirect(redirectRenderName);
    });
  }
});


module.exports = router;