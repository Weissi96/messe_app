const express = require('express');
const router = express.Router();
var common = require('./common');
var database = require('../database/database');

// translation
var renderName = 'neueinteresse';
var titleEdit = 'Interesse bearbeiten';
var titleAdd = 'Interesse hinzufügen';
var successEdit = 'Interesse erfolgreich bearbeitet';
var successAdd = 'Interesse erfolgreich hinzugefügt';

router.get('/', (req, res) => {
  var query_id = req.query.id;
  if(query_id){
    database.getInteresseById(query_id, function(error, result) {
      var fachgebiet = { fachgebietId:result[0].FachgebietId, fachgebiet:result[0].Fachgebiet};
      return res.render(renderName, { title: titleEdit, user: common.getUser(req), fachgebiet: fachgebiet });
    });
  } else {
    res.render(renderName, { title: titleAdd, user: common.getUser(req) });
  }
});

router.post('/', (req, res) => {
  var post  = req.body;
  var fachgebiet = post.fachgebiet;;
  var id = post.id;
  if(post.submit == titleEdit){
    database.updateInteresse(id, fachgebiet, function(error, result) {
      var keyVal = { successEdit };
      res.render(renderName, { title: titleAdd, user: common.getUser(req), successmessages: keyVal });
    });
  } else {
    database.createInteresse(fachgebiet, function(error, result) {
          var keyVal = { successAdd };
          res.render(renderName, { title: titleAdd, user: common.getUser(req), successmessages: keyVal });
    });
  }
});

module.exports = router;