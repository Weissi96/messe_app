const express = require('express');
const router = express.Router();
var common = require('./common');
var database = require('../database/database');

// translation
var renderName = 'neueausbildung';
var titleEdit = 'Ausbildung bearbeiten';
var titleAdd = 'Ausbildung hinzufügen';
var successEdit = 'Ausbildung erfolgreich bearbeitet';
var successAdd = 'Ausbildung erfolgreich hinzugefügt';

router.get('/', (req, res) => {
  var query_id = req.query.id;
  if(query_id){
    database.getAusbildungById(query_id, function(error, result) {
      var ausbildung = { ausbildungId:result[0].AusbildungId, ausbildung:result[0].Ausbildung};
      init(ausbildung, titleEdit, req, res);
    });
  } else {
    init(null, titleAdd, req, res);
  }
});

function init(ausbildung, title, req, res) {
  return res.render(renderName, { title: title, user: common.getUser(req), ausbildung: ausbildung });
}

router.post('/', (req, res) => {
  var post  = req.body;
  var ausbildung = post.ausbildung;
  var id = post.id;
  if(post.submit == titleEdit){
    database.updateAusbildung(id, ausbildung, function(error, result) {
      var keyVal = { successEdit};
      res.render(renderName, { title: titleAdd, user: common.getUser(req), successmessages: keyVal });
    });
  } else {
    database.createAusbildung(ausbildung, function(error, result) {
          var keyVal = { successAdd };
          res.render(renderName, { title: titleAdd, user: common.getUser(req), successmessages: keyVal });
    });
  }
});

module.exports = router;