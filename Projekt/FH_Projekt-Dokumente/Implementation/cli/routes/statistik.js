const express = require('express');
const router = express.Router();
var common = require('../routes/common');
var http = require('http');
var fs = require('fs');

// translation
var renderName = 'statistik';
var title = 'Statistik';

router.get('/', (req, res) => {
  fs.readFile('./views/index_statistik_gesamt.html', function (err, html) {
    if (err) {
        throw err; 
    }    
    res.write(html);   
  });
});



module.exports = router;