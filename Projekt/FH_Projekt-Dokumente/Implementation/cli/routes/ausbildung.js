const express = require('express');
const router = express.Router();
const database = require('../database/database');
var common = require('./common');

// translation
var renderName = 'ausbildung';
var title = 'ausbildung';
var successDelete = 'Ausbildung erfolgreich gelöscht';
var titleButtonEdit = 'Bearbeiten';

router.get('/', (req, res) => {
  init(null, req, res);
});

router.post('/', (req, res) => {
  var post  = req.body;
  var id = post.id;
  if(post.submit == titleButtonEdit){
    res.redirect('/neueausbildung?id=' + id);
  } else {
    database.deleteAusbildung(id,function(error, result) {
        var keyVal = { successDelete };
        init(keyVal, req, res);
    });
  }
});

function init(messages, req, res){
  database.getAusbildung(function(error, result) {
    var keyVal = {};
    Object.keys(result).forEach(function(key) {
      var row = result[key];
      keyVal[row.AusbildungId] = row.Ausbildung;
    });
    res.render(renderName, { title: title, user: common.getUser(req), ausbildung: keyVal, successmessages: messages });
  });
}

module.exports = router;