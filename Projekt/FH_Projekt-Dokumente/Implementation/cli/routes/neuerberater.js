const express = require('express');
const router = express.Router();
var common = require('../routes/common');
var database = require('../database/database');

// translation
var renderName = 'neuerberater';
var redirectRenderName = 'berater';
var titleEdit = 'Berater bearbeiten';
var titleAdd = 'Berater hinzufügen';

router.get('/', (req, res) => {
  var query_id = req.query.id;
  if(query_id){
    database.getBeraterById(query_id, function(error, result) {
      var berater = { beraterId:result[0].BeraterId, vorname:result[0].Vorname, nachname:result[0].Nachname, email:result[0].EMail, matrikelnr:result[0].MatrikelNr, passwort:result[0].Passwort, beratertypid:result[0].BeratertypId, campusId:result[0].CampusId, studiengangId:result[0].StudiengangId};
      init(berater, titleEdit, req, res);
    });
  } else {
    init(null, titleAdd, req, res);
  }
});

function init(berater, title, req, res) {
  database.fillCreateBerater(function(error, result) {
    var beraterTyp = {};
    var campus = {};
    var studiengang = {};
    Object.keys(result).forEach(function(key) {
      switch (key) {
        case '0':
          Object.keys(result[key]).forEach(function(keyBeraterTyp) {
            var row = result[key][keyBeraterTyp];
            beraterTyp[keyBeraterTyp] = { name: row.Beratertyp, id: row.BeratertypId };
          });
          break;
          case '1':
            Object.keys(result[key]).forEach(function(keyCampus) {
              var row = result[key][keyCampus];
              campus[keyCampus] = { name: row.Campus, id: row.CampusId };
            });
            break;
          case '2':
            Object.keys(result[key]).forEach(function(keyStudiengang) {
              var row = result[key][keyStudiengang];
              studiengang[keyStudiengang] = { name: row.Studiengang, id: row.StudiengangId, campusid: row.CampusId };
            });
            break;
      }
    });
    return res.render(renderName, { title: title, user: common.getUser(req), berater: berater, beraterTyp: beraterTyp, campus: campus, studiengang: studiengang });
  });
}

router.post('/', (req, res) => {
  var post  = req.body;
  var vorname = post.vorname;
  var nachname = post.nachname;
  var email = post.email;
  var passwort = post.passwort;
  var matrikelnr = post.matrikelnr;
  var beratertyp = post.beratertyp;
  var campusId = post.campusId;
  var studiengangId = post.studiengangId;
  var id = post.id;
  if(post.submit == titleEdit){
    database.updateBerater(id,vorname, nachname, email, passwort, matrikelnr, beratertyp, campusId, studiengangId, function(error, result) {
      res.redirect(redirectRenderName);
    });
  } else {
    database.createBerater(vorname, nachname, email, passwort, matrikelnr, beratertyp, campusId, studiengangId, function(error, result) {
      res.redirect(redirectRenderName);
    });
  }
});


module.exports = router;