const express = require('express');
const router = express.Router();
var common = require('./common');
var database = require('../database/database');

// translation
var renderName = 'neuercampus';
var titleEdit = 'Campus bearbeiten';
var titleAdd = 'Campus hinzufügen';
var successEdit = 'Campus erfolgreich bearbeitet';
var successAdd = 'Campus erfolgreich hinzugefügt';

router.get('/', (req, res) => {
  var query_id = req.query.id;
  if(query_id){
    database.getCampusById(query_id, function(error, result) {
      var campus = { campusId:result[0].CampusId, campus:result[0].Campus , beschreibung:result[0].Beschreibung};
      return res.render(renderName, { title: titleEdit, user: common.getUser(req), campus: campus });
    });
  } else {
    res.render(renderName, { title: titleAdd, user: common.getUser(req) });
  }
});

router.post('/', (req, res) => {
  var post  = req.body;
  var campus = post.campus;
  var beschreibung = post.beschreibung;
  var id = post.id;
  if(post.submit == titleEdit){
    database.updateCampus(id, campus, beschreibung, function(error, result) {
      var keyVal = { successEdit };
      res.render(renderName, { title: titleAdd, user: common.getUser(req), successmessages: keyVal });
    });
  } else {
    database.createCampus(campus, beschreibung, function(error, result) {
          var keyVal = { successAdd };
          res.render(renderName, { title: titleAdd, user: common.getUser(req), successmessages: keyVal });
    });
  }
});


module.exports = router;