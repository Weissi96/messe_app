const express = require('express');
const router = express.Router();
const database = require('../database/database');
var common = require('../routes/common');

// translation
var renderName = 'campus';
var title = 'Campus';
var successDelete = 'Campus erfolgreich gelöscht';
var titleButtonEdit = 'Bearbeiten';

router.get('/', (req, res) => {
  init(null, req, res);
});

router.post('/', (req, res) => {
  var post  = req.body;
  var id = post.id;
  if(post.submit == titleButtonEdit){
    res.redirect('/neuercampus?id=' + id);
  } else {
    database.deleteCampus(id,function(error, result) {
        var keyVal = { successDelete };
        init(keyVal, req, res);
    });
  }
});

function init(messages, req, res){
  database.getCampus(function(error, result) {
    var keyVal = {};
    Object.keys(result).forEach(function(key) {
      var row = result[key];
      keyVal[row.CampusId] = row.Campus;
    });
    res.render(renderName, { title: title, user: common.getUser(req), campus: keyVal, successmessages: messages });
  });
}

module.exports = router;