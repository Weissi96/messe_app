const express = require('express');
const router = express.Router();
var common = require('../routes/common');
const database = require('../database/database');

// translation
var renderName = 'berater';
var title = 'Berater';
var successDelete = 'Berater erfolgreich gelöscht';
var titleButtonEdit = 'Bearbeiten';

router.get('/', (req, res) => {
  init(null, req, res);
});

router.post('/', (req, res) => {
  var post  = req.body;
  var id = post.id;
  if(post.submit == titleButtonEdit){
    res.redirect('/neuerberater?id=' + id);
  } else {
    database.deleteBerater(id,function(error, result) {
        var keyVal = { successDelete };
        init(keyVal, req, res);
    });
  }
});

function init(messages, req, res){
  database.getBerater(function(error, result) {
    var keyVal = {};
    Object.keys(result).forEach(function(key) {
      var row = result[key];
      keyVal[row.BeraterId] = row.Vorname + " " + row.Nachname;
    });
    res.render(renderName, { title: title, user: common.getUser(req), berater: keyVal, successmessages: messages });
  });
}

module.exports = router;