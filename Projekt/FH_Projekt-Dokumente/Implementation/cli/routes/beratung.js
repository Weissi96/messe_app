const express = require('express');
const router = express.Router();
const database = require('../database/database');
var common = require('../routes/common');

// translation
var renderName = 'beratung';
var title = 'Beratung';

router.get('/', (req, res) => {
  var query_id = req.query.id;
  database.fillBeratung(query_id,function(error, result) {
    var beratungen = {};
    Object.keys(result).forEach(function(key) {
      switch (key) {
        case '0':
          Object.keys(result[key]).forEach(function(keySession) {
            var row = result[key][keySession];
            beratungen[row.SessionId] = row.Vorname + ' ' + row.Nachname + ', Berater: ' + row.BVorname + ' ' + row.BNachname;
          });
          break;
      }
    });
    res.render(renderName, { title: title, user: common.getUser(req), id : query_id, beratungen: beratungen });
  });
});

module.exports = router;