const express = require('express');
const router = express.Router();
const database = require('../database/database');
var common = require('./common');

// translation
var renderName = 'studiengangdetail';
var title = 'Studiengang';
var titleButtonEdit = 'Bearbeiten';

router.get('/', (req, res) => {
  init(null, req, res);
});

router.post('/', (req, res) => {
  var post  = req.body;
  var id = post.id;
  if(post.submit == titleButtonEdit){
    res.redirect('/neuerstudiengang?id=' + id);
  } else {
    database.deleteStudiengang(id,function(error, result) {
        var keyVal = { successDelete };
        init(keyVal, req, res);
    });
  }
});

function init(messages, req, res){
  var query_id = req.query.id;
  database.getStudiengangById(query_id,function(error, result) {
    var name = result[0].Studiengang;
    var beschreibung = result[0].Beschreibung;
    res.render(renderName, { title: title, user: common.getUser(req), name: name, beschreibung: beschreibung, successmessages: messages });
  });
}

module.exports = router;