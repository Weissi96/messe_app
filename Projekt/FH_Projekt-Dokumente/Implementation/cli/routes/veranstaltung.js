const express = require('express');
const router = express.Router();
const database = require('../database/database');
var common = require('../routes/common');

// translation
var renderName = 'veranstaltung';
var title = 'Veranstaltung';

router.get('/', (req, res) => {
  var query_id = req.query.id;
  database.fillVeranstaltung(query_id,function(error, result) {
    var veranstaltung = {};
    var beratungen = {};
    var berater = {};
    var alleBerater = {};
    Object.keys(result).forEach(function(key) {
      switch (key) {
        case '0':
          var row = result[key][0];
          var von = common.toDateTime(row.Von);
          var bis = common.toDateTime(row.Bis);
          veranstaltung = [row.VeranstaltungId,row.Veranstaltung, von, bis,row.Veranstalter, row.Standort, row.Sujet, row.Briefing];
          break;
        case '1':
          Object.keys(result[key]).forEach(function(keyBeratung) {
            var row = result[key][keyBeratung];
            beratungen[row.BeratungId] = {datum:row.Von, anzahl:row.Anzahl};
          });
          break;
        case '2':
          Object.keys(result[key]).forEach(function(keyBerater) {
            var row = result[key][keyBerater];
            berater[row.BeraterId] = {name:row.Nachname, id:row.BeraterId};
          });
          break;
        case '3':
          Object.keys(result[key]).forEach(function(keyAlleBerater) {
            var row = result[key][keyAlleBerater];
            alleBerater[row.BeraterId] = {name:row.Nachname, id:row.BeraterId};
          });
          break;
      }
    });
    res.render(renderName, { title: title, user: common.getUser(req), id : query_id, beratungen: beratungen, veranstaltung: veranstaltung, berater: berater, alleBerater: alleBerater });
  });
});

router.post('/', (req, res) => {
  var post  = req.body;
  if(post.submit == "Entfernen"){
    var beraterId = post.bid;
    var veranstaltungId = post.vid;
    database.removeBeraterByVeranstaltungsId(beraterId, veranstaltungId, function(error, result) {
      res.redirect("/veranstaltung?id=" + veranstaltungId);
    });
  } else {
    var beraterId = post.selectedBerater;
    var veranstaltungId = post.id[0];
    database.addBeraterByVeranstaltungsId(beraterId, veranstaltungId, "13579", function(error, result) {
      res.redirect("/veranstaltung?id=" + veranstaltungId);
    });
  }
});

module.exports = router;