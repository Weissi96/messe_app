const express = require('express');
const router = express.Router();
var common = require('../routes/common');
var database = require('../database/database');

// translation
var renderName = 'onestopflyer';
var title = 'Flyer';

router.get('/', (req, res) => {
  var query_id = req.query.id;
  database.getOneStopFlyerInfoById(query_id, function(error, result) { 
    var text = 'Hallo ' + result[0][0].Vorname ;
    console.log(result);
    res.render(renderName, { title: title, user: common.getUser(req), text: text  });
  });
});

module.exports = router;