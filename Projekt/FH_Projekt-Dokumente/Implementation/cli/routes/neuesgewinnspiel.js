const express = require('express');
const router = express.Router();
var common = require('./common');
var database = require('../database/database');

// translation
var renderName = 'neuesgewinnspiel';
var titleEdit = 'Gewinnspiel bearbeiten';
var titleAdd = 'Gewinnspiel hinzufügen';
var successEdit = 'Gewinnspiel erfolgreich bearbeitet';
var successAdd = 'Gewinnspiel erfolgreich hinzugefügt';

router.get('/', (req, res) => {
  var query_id = req.query.id;
  if(query_id){
    database.getGewinnspielById(query_id, function(error, result) {
      var gewinnspiel = { gewinnspielId:result[0].GewinnspielId, gewinnspiel:result[0].Gewinnspiel, beschreibung:result[0].Beschreibung, kürzel:result[0].Kürzel};
      init(gewinnspiel, titleEdit, req, res);
    });
  } else {
    init(null, titleAdd, req, res);
  }
});

function init(gewinnspiel, title, req, res) {
  return res.render(renderName, { title: title, user: common.getUser(req), gewinnspiel: gewinnspiel });
}

router.post('/', (req, res) => {
  var post  = req.body;
  var gewinnspiel = post.gewinnspiel;
  var beschreibung = post.beschreibung;
  var id = post.id;
  if(post.submit == titleEdit){
    database.updateGewinnspiel(id, gewinnspiel, beschreibung, function(error, result) {
      var keyVal = { successEdit };
      res.render(renderName, { title: titleAdd, user: common.getUser(req), successmessages: keyVal });
    });
  } else {
    database.createGewinnspiel(gewinnspiel, beschreibung, function(error, result) {
          var id = result[0][0].GewinnspielId;
          var keyVal = { successAdd };
          res.render(renderName, { title: titleAdd, user: common.getUser(req), successmessages: keyVal });
    });
  }
});

module.exports = router;