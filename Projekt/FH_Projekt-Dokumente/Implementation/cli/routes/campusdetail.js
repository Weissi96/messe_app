const express = require('express');
const router = express.Router();
const database = require('../database/database');
var common = require('./common');

// translation
var renderName = 'campusdetail';
var title = 'Campus';
var successDelete = 'Studiengang erfolgreich gelöscht';
var titleButtonEdit = 'Bearbeiten';

router.get('/', (req, res) => {
  var query_id = req.query.id;
  init(null, req, res, query_id);
});

router.post('/', (req, res) => {
  var post  = req.body;
  var id = post.id;
  var campusId = post.campusId;
  if(post.submit == titleButtonEdit){
    res.redirect('/neuerstudiengang?id=' + id);
  } else {
    database.deleteStudiengang(id,function(error, result) {
        var keyVal = { successDelete };
        init(keyVal, req, res, campusId);
    });
  }
});

function init(messages, req, res, id){
  database.getCampusById(id,function(error, result) {
    var name = result[0].Campus;
    var campusId = result[0].CampusId;
    var beschreibung = result[0].Beschreibung;
    database.getStudiengangByCampus(id,function(error, result) {
      var studiengang = {};
      Object.keys(result).forEach(function(key) {
        var row = result[key];
        studiengang[row.StudiengangId] = row.Studiengang + "  " + row.Kürzel;
      });
      res.render(renderName, { title: title, user: common.getUser(req), name: name, beschreibung: beschreibung, successmessages: messages, studiengang: studiengang, campusId: campusId });
    });
  });
}

module.exports = router;