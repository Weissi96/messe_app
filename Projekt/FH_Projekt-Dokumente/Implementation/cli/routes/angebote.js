const express = require('express');
const router = express.Router();
const database = require('../database/database');
var common = require('./common');

// translation
var renderName = 'angebote';
var title = 'Angebote';
var successDelete = 'Angebot erfolgreich gelöscht';
var titleButtonEdit = 'Bearbeiten';

router.get('/', (req, res) => {
  init(null, req, res);
});

router.post('/', (req, res) => {
  var post  = req.body;
  var id = post.id;
  if(post.submit == titleButtonEdit){
    res.redirect('/neueangebote?id=' + id);
  } else {
    database.deleteAngebot(id,function(error, result) {
      var keyVal = { successDelete };
      init(keyVal, req, res);
    });
  }
});

function init(messages, req, res){
  database.getAngebote(function(error, result) {
    var keyVal = {};
    Object.keys(result).forEach(function(key) {
      var row = result[key];
      keyVal[row.AngebotId] = row.Angebot;
    });
    res.render(renderName, { title: title, user: common.getUser(req), angebote: keyVal, successmessages: messages });
  });
}

module.exports = router;