const express = require('express');
const router = express.Router();
const database = require('../database/database');
var common = require('../routes/common');

// translation
var renderName = 'beraterdetail';
var title = 'Berater';

router.get('/', (req, res) => {
  var query_id = req.query.id; 
  database.fillBerater(query_id,function(error, result) {
    var berater = {};
    var beratungen = {};
    Object.keys(result).forEach(function(key) {
      switch (key) {
        case '0':
          var row = result[key][0];
          berater = [row.BeraterId,row.Vorname, row.Nachname, row.EMail,row.MatrikelNr, row.Campus, row.Studiengang, row.Beratertyp];
          break;
        case '1':
          Object.keys(result[key]).forEach(function(keySession) {
            var row = result[key][keySession];
            beratungen[row.SessionId] = row.Vorname + ' ' + row.Nachname;
          });
          break;
      }
    });
    res.render(renderName, { title: title, user: common.getUser(req), id : query_id, berater: berater, beratungen: beratungen });
  });
});

module.exports = router;