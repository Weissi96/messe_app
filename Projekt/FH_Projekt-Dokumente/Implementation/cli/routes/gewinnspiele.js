const express = require('express');
const router = express.Router();
const database = require('../database/database');
var common = require('./common');

// translation
var renderName = 'gewinnspiele';
var title = 'gewinnspiele';
var successDelete = 'Gewinnspiel erfolgreich gelöscht';
var titleButtonEdit = 'Bearbeiten';

router.get('/', (req, res) => {
  init(null, req, res);
});

router.post('/', (req, res) => {
  var post  = req.body;
  var id = post.id;
  if(post.submit == titleButtonEdit){
    res.redirect('/neuesgewinnspiel?id=' + id);
  } else {
    database.deleteGewinnspiel(id,function(error, result) {
        var keyVal = { successDelete };
        init(keyVal, req, res);
    });
  }
});

function init(messages, req, res){
  database.getGewinnspiel(function(error, result) {
    var keyVal = {};
    Object.keys(result).forEach(function(key) {
      var row = result[key];
      keyVal[row.GewinnspielId] = row.Gewinnspiel;
    });
    res.render(renderName, { title: title, user: common.getUser(req), gewinnspiel: keyVal, successmessages: messages });
  });
}

module.exports = router;