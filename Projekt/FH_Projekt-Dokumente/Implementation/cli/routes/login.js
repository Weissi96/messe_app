const express = require('express');
const router = express.Router();
var common = require('../routes/common');

var renderName = 'login';
var title = 'Login';

router.get('/', (req, res) => {
  res.render(renderName, { title: title, user: common.getUser(req) });
});

module.exports = router;