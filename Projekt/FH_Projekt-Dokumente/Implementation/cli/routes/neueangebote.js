const express = require('express');
const router = express.Router();
var common = require('./common');
var database = require('../database/database');

// translation
var renderName = 'neueangebote';
var titleEdit = 'Angebot bearbeiten';
var titleAdd = 'Angebot hinzufügen';
var successEdit = 'Angebot erfolgreich bearbeitet';
var successAdd = 'Angebot erfolgreich hinzugefügt';

router.get('/', (req, res) => {
  var query_id = req.query.id;
  if(query_id){
    database.getAngbebotById(query_id, function(error, result) {
      var angebot = { angebotId:result[0].AngebotId, angebot:result[0].Angebot, beschreibung:result[0].Beschreibung};
      return res.render(renderName, { title: titleEdit, user: common.getUser(req), angebot: angebot });
    });
  } else {
    res.render(renderName, { title: titleAdd, user: common.getUser(req) });
  }
});

router.post('/', (req, res) => {
  var post  = req.body;
  var angebot = post.angebot;
  var beschreibung = post.beschreibung;
  var id = post.id;
  if(post.submit == titleEdit){
    database.updateAngebot(id, angebot, beschreibung, function(error, result) {
      var keyVal = { successEdit };
      res.render(renderName, { title: titleAdd, user: common.getUser(req), successmessages: keyVal });
    });
  } else {
    database.createAngebot(angebot, beschreibung, function(error, result) {
          var keyVal = { successAdd };
          res.render(renderName, { title: titleAdd, user: common.getUser(req), successmessages: keyVal });
    });
  }
});


module.exports = router;