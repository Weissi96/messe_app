const express = require('express');
const router = express.Router();
const database = require('../database/database');
var common = require('./common');

// translation
var renderName = 'schule';
var title = 'Schule';
var successDelete = 'Schule erfolgreich gelöscht';
var titleButtonEdit = 'Bearbeiten';

router.get('/', (req, res) => {
  init(null, req, res);
});

router.post('/', (req, res) => {
  var post  = req.body;
  var id = post.id;
  if(post.submit == titleButtonEdit){
    res.redirect('/neueschule?id=' + id);
  } else {
    database.deleteSchule(id,function(error, result) {
      var keyVal = { successDelete };
      init(keyVal, req, res);
    });
  }
});

function init(messages, req, res){
  database.getSchulen(function(error, result) {
    var keyVal = {};
    Object.keys(result).forEach(function(key) {
      var row = result[key];
      keyVal[row.SchuleId] = row.Schule;
    });
    res.render(renderName, { title: title, user: common.getUser(req), schulen: keyVal, successmessages: messages });
  });
}

module.exports = router;