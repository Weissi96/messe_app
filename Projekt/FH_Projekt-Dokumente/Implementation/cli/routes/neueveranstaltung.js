const express = require('express');
const router = express.Router();
var common = require('../routes/common');
var database = require('../database/database');

// translation
var renderName = 'neueveranstaltung';
var redirectRenderName = 'veranstaltungen';
var titleEdit = 'Veranstaltung bearbeiten';
var titleAdd = 'Veranstaltung hinzufügen';

router.get('/', (req, res) => {
  var query_id = req.query.id;
  if(query_id){
    database.getVeranstaltungById(query_id, function(error, result) { 
      var datetimeVon = common.toDateTimeArray(result[0].Von);
      var datetimeBis = common.toDateTimeArray(result[0].Bis);
      var veranstaltung = { veranstaltungId:result[0].VeranstaltungId, name:result[0].Veranstaltung, von:result[0].Von, bis:result[0].Bis, kosten:result[0].Kosten, sujetId:result[0].SujetId , briefingId:result[0].BriefingId, standortId:result[0].StandortId, veranstalterId:result[0].VeranstalterId, vondate:datetimeVon[0], vontime:datetimeVon[1], bisdate:datetimeBis[0], bistime:datetimeBis[1]};
      init(veranstaltung, titleEdit, req, res);
    });
  } else {
    init(null, titleAdd, req, res);
  }
});


function init(veranstaltung, title, req, res) {
  database.fillCreateVeranstaltung(function(error, result) {
    var veranstalter = {};
    var standort = {};
    var sujet = {};
    var briefing = {}; 
    Object.keys(result).forEach(function(key) {
      switch (key) {
        case '0':
          Object.keys(result[key]).forEach(function(keyVeranstalter) {
            var row = result[key][keyVeranstalter];
            veranstalter[keyVeranstalter] = { name: row.Veranstalter, id: row.VeranstalterId };
          });
          break;
        case '1':
          Object.keys(result[key]).forEach(function(keyStandort) {
            var row = result[key][keyStandort];
            standort[keyStandort] = { name: row.Standort, id: row.StandortId };
          });
          break;
        case '2':
          Object.keys(result[key]).forEach(function(keySujet) {
            var row = result[key][keySujet];
            sujet[keySujet] = { name: row.Sujet, id: row.SujetId };
          });
          break;
        case '3':
          Object.keys(result[key]).forEach(function(keyBriefing) {
            var row = result[key][keyBriefing];
            briefing[keyBriefing] = { name: row.Briefing, id: row.BriefingId };
          });
          break;
      }
    });
    return res.render(renderName, { title: title, user: common.getUser(req), veranstaltung: veranstaltung, veranstalter: veranstalter, sujet: sujet, standort: standort, briefing: briefing });
  });
}

router.post('/', (req, res) => {
  var post  = req.body;
  var name = post.name;
  var vondate = post.vondate;
  var bisdate = post.bisdate;
  var vontime = post.vontime;
  var bistime = post.bistime;
  var kosten = post.kosten;
  var veranstalter = post.veranstalter;
  var standort = post.standort;
  var sujet = post.sujet;
  var briefing = post.briefing;
  var id = post.id;

  var von = vondate + ' ' + vontime;
  var bis = bisdate + ' ' + bistime;
  
  if(post.submit == titleEdit){
    database.updateVeranstaltung(id,name, von, bis, kosten, veranstalter, standort, sujet, briefing, function(error, result) {
      res.redirect(redirectRenderName);
    });
  } else {
    database.createVeranstaltung(name, von, bis, kosten, veranstalter, standort, sujet, briefing, function(error, result) {
      res.redirect(redirectRenderName);
    });
  }
});

module.exports = router;