const express = require('express');
const router = express.Router();
var common = require('./common');
var database = require('../database/database');

// translation
var renderName = 'neuerstudiengang';
var titleEdit = 'Studiengang bearbeiten';
var titleAdd = 'Studiengang hinzufügen';
var successEdit = 'Studiengang erfolgreich bearbeitet';
var successAdd = 'Studiengang erfolgreich hinzugefügt';

router.get('/', (req, res) => {
  var query_id = req.query.id;
  if(query_id){
    database.getStudiengangById(query_id, function(error, result) {
      var studiengang = { studiengangId:result[0].StudiengangId, studiengang:result[0].Studiengang, beschreibung:result[0].Beschreibung, kürzel:result[0].Kürzel, abschlussId: result[0].AbschlussId, campusId: result[0].CampusId};
      init(studiengang, titleEdit, req, res);
    });
  } else {
    init(null, titleAdd, req, res);
  }
});

function init(studiengang, title, req, res) {
  database.fillCreateStudiengang(function(error, result) {
    var campus = {};
    var abschluss = {};
    Object.keys(result).forEach(function(key) {
      switch (key) {
        case '0':
          Object.keys(result[key]).forEach(function(keyCampus) {
            var row = result[key][keyCampus];
            campus[keyCampus] = { name: row.Campus, id: row.CampusId };
          });
          break;
        case '1':
          Object.keys(result[key]).forEach(function(keyAbschluss) {
            var row = result[key][keyAbschluss];
            abschluss[keyAbschluss] = { name: row.Abschluss, id: row.AbschlussId };
          });
          break;
      }
    });
    return res.render(renderName, { title: title, user: common.getUser(req), studiengang: studiengang, campus: campus, abschluss: abschluss });
  });
}

router.post('/', (req, res) => {
  var post  = req.body;
  var studiengang = post.studiengang;
  var beschreibung = post.beschreibung;
  var kürzel = post.kürzel;
  var id = post.id;
  var abschluss = post.abschluss;
  var campus = post.campus;
  if(post.submit == titleEdit){
    database.updateStudiengang(id, kürzel, studiengang, beschreibung, abschluss, campus, function(error, result) {
      var keyVal = { successEdit };
      res.render(renderName, { title: titleAdd, user: common.getUser(req), successmessages: keyVal });
    });
  } else {
    database.createStudiengang(kürzel, studiengang, beschreibung, abschluss, campus, function(error, result) {
          var keyVal = { successAdd };
          res.render(renderName, { title: titleAdd, user: common.getUser(req), successmessages: keyVal });
    });
  }
});

module.exports = router;