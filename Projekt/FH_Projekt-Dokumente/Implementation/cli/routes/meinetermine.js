const express = require('express');
const router = express.Router();
var common = require('../routes/common');

// translation
var renderName = 'meinetermine';
var title = 'Meine Termine';

router.get('/', (req, res) => {
  res.render(renderName, { title: title, user: common.getUser(req) });
});

module.exports = router;