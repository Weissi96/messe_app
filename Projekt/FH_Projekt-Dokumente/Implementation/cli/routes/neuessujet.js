const express = require('express');
const router = express.Router();
var common = require('./common');
var database = require('../database/database');

// translation
var renderName = 'neuessujet';
var redirectRenderName = 'sujet';
var titleEdit = 'Sujet bearbeiten';
var titleAdd = 'Sujet hinzufügen';

router.get('/', (req, res) => {
  var query_id = req.query.id;
  if(query_id){
    database.getSujetById(query_id, function(error, result) {
      var sujet = { sujetId:result[0].SujetId, sujet:result[0].Sujet};
      return res.render(renderName, { title: titleEdit, user: common.getUser(req), sujet: sujet });
    });
  } else {
    res.render(renderName, { title: titleAdd, user: common.getUser(req) });
  }
});

router.post('/', (req, res) => {
  var post  = req.body;
  var sujet = post.sujet;;
  var id = post.id;
  if(post.submit == titleEdit){
    database.updateSujet(id, sujet, function(error, result) {
      res.redirect(redirectRenderName);
    });
  } else {
    database.createSujet(sujet, function(error, result) {
          res.redirect(redirectRenderName);
    });
  }
});

module.exports = router;