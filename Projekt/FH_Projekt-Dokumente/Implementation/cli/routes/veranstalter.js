const express = require('express');
const router = express.Router();
const database = require('../database/database');
var common = require('./common');

// translation
var renderName = 'veranstalter';
var editButton = 'Bearbeiten';
var title = 'Veranstalter';
var successDelete = 'Veranstalter erfolgreich gelöscht';

router.get('/', (req, res) => {
  init(null, req, res);
});

router.post('/', (req, res) => {
  var post  = req.body;
  var id = post.id;
  if(post.submit == editButton){
    res.redirect('/neuerveranstalter?id=' + id);
  } else {
    database.deleteVeranstalter(id,function(error, result) {
        var keyVal = { successDelete };
        init(keyVal, req, res);
    });
  }
});

function init(messages, req, res){
  database.getVeranstalter(function(error, result) {
    var keyVal = {};
    Object.keys(result).forEach(function(key) {
      var row = result[key];
      keyVal[row.VeranstalterId] = row.Veranstalter;
    });
    res.render(renderName, { title: title, user: common.getUser(req), veranstalter: keyVal, successmessages: messages });
  });
}

module.exports = router;