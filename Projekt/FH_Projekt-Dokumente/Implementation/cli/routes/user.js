const database = require('../database/database');

// translation
var wrongCredentials = "Wrong Credentials";

exports.login = function(req, res){
   if(req.method == "POST"){
      var post  = req.body;
      var email= post.email;
      var pass= post.password;

      database.loginUser(email, pass, function(error, result) {
        if(result.length){
            req.session.userId = result[0].BeraterId;
            req.session.user = result[0];
            res.redirect('/');
         }
         else{
            var keyVal = { wrongCredentials };
            res.render('login', { title: 'Login', errormessages: keyVal });
         }
      });
   } else {
        res.redirect('/');
   }         
};

exports.logout = function(req, res){
    req.session.destroy(function(err) {
        res.redirect('/');
    })       
};

exports.getUser = function(req){
    return getUser(req);      
};

function getUser(req) {
    var user = req.session.user;
    if (user) {
        return { name: user.Vorname, id: user.BeraterId };
    }
    return undefined;
} 