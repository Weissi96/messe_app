const express = require('express');
const router = express.Router();
const database = require('../database/database');
var common = require('./common');

// translation
var renderName = 'interessen';
var title = 'Interesse';
var successDelete = 'Interesse erfolgreich gelöscht';
var titleButtonEdit = 'Bearbeiten';

router.get('/', (req, res) => {
  init(null, req, res);
});

router.post('/', (req, res) => {
  var post  = req.body;
  var id = post.id;
  if(post.submit == titleButtonEdit){
    res.redirect('/neueinteresse?id=' + id);
  } else {
    database.deleteInteresse(id,function(error, result) {
        var keyVal = { successDelete };
        init(keyVal, req, res);
    });
  }
});

function init(messages, req, res){
  database.getInteressen(function(error, result) {
    var keyVal = {};
    Object.keys(result).forEach(function(key) {
      var row = result[key];
      keyVal[row.FachgebietId] = row.Fachgebiet;
    });
    res.render(renderName, { title: title, user: common.getUser(req), interessen: keyVal, successmessages: messages });
  });
}

module.exports = router;