const express = require('express');
const bodyParser = require('body-parser');
const database = require('./database/database');
const app = express();

// toures
var user = require('./routes/user');
var index = require('./routes/index');
var login = require('./routes/login');
var campus = require('./routes/campus');
var campusdetail = require('./routes/campusdetail');
var neuercampus = require('./routes/neuercampus');
var studiengang = require('./routes/studiengang');
var studiengangdetail = require('./routes/studiengangdetail');
var neuerstudiengang = require('./routes/neuerstudiengang');
var berater = require('./routes/berater');
var veranstaltungen = require('./routes/veranstaltungen');
var veranstaltung = require('./routes/veranstaltung');
var statistik = require('./routes/statistik');
var onestopflyer = require('./routes/onestopflyer');
var meinedaten = require('./routes/meinedaten');
var meinetermine = require('./routes/meinetermine');
var neueveranstaltung = require('./routes/neueveranstaltung');
var neuerberater = require('./routes/neuerberater');
var beraterdetail = require('./routes/beraterdetail');
var beratung = require('./routes/beratung');
var kontakt = require('./routes/kontakt');
var impressum = require('./routes/impressum');
var interessen = require('./routes/interessen');
var neueinteresse = require('./routes/neueinteresse');
var angebote = require('./routes/angebote');
var neueangebote = require('./routes/neueangebote');
var veranstalter = require('./routes/veranstalter');
var neuerveranstalter = require('./routes/neuerveranstalter');
var sujet = require('./routes/sujet');
var neuessujet = require('./routes/neuessujet');
var gewinnspiele = require('./routes/gewinnspiele');
var neuesgewinnspiel = require('./routes/neuesgewinnspiel');
var ausbildung = require('./routes/ausbildung');
var neueausbildung = require('./routes/neueausbildung');
var schule = require('./routes/schule');
var neueschule = require('./routes/neueschule');

app.set('view engine', 'pug');

app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

app.use(express.static('public'));

var session = require('express-session');
app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true,
    cookie: { maxAge: 60000 }
  }))

app.use('/', index);
app.post('/login', user.login);//call for login post
app.get('/logout', user.logout);//call for login post
app.use('/login', login);
app.use('/berater', berater);
app.use('/statistik', statistik);
app.use('/veranstaltungen', veranstaltungen);
app.use('/veranstaltung', veranstaltung);
app.use('/studiengang', studiengang);
app.use('/studiengangdetail', studiengangdetail);
app.use('/neuerstudiengang', neuerstudiengang);
app.use('/campus', campus);
app.use('/campusdetail', campusdetail);
app.use('/neuercampus', neuercampus);
app.use('/onestopflyer', onestopflyer);
app.use('/meinedaten', meinedaten);
app.use('/meinetermine', meinetermine);
app.use('/neueveranstaltung', neueveranstaltung);
app.use('/neuerberater', neuerberater);
app.use('/beraterdetail', beraterdetail);
app.use('/beratung', beratung);
app.use('/impressum', impressum);
app.use('/kontakt', kontakt);
app.use('/interessen', interessen);
app.use('/neueinteresse', neueinteresse);
app.use('/angebote', angebote);
app.use('/neueangebote', neueangebote);
app.use('/veranstalter', veranstalter);
app.use('/neuerveranstalter', neuerveranstalter);
app.use('/sujet', sujet);
app.use('/neuessujet', neuessujet);
app.use('/gewinnspiele', gewinnspiele);
app.use('/neuesgewinnspiel', neuesgewinnspiel);
app.use('/ausbildung', ausbildung);
app.use('/neueausbildung', neueausbildung);
app.use('/schule', schule);
app.use('/neueschule', neueschule);

app.get("/api/login", (req, res, next) => {
    database.loginByPassword(req.query.passwort, function(error, result) {
      var veranstaltungId = 0;
      var beraterId = 0;
      var briefing = 0;
      var von = 0;
      var bis = 0;
      var interessen = {};
      var angebote = {};
      var schule = {};
      var länder = {};
      var schultypen = {};
      var geschlechter = {};
      var campus = {};

      Object.keys(result).forEach(function(key) {
        switch (key) {
          case '0':
            Object.keys(result[key]).forEach(function(keyData) {
              var row = result[key][keyData];
              console.log(row);
              veranstaltungId = row.VeranstaltungId;
              beraterId = row.BeraterId;
              briefing = row.Briefing;
              von = row.Von;
              bis = row.Bis;
            });
            break;
          case '1':
            Object.keys(result[key]).forEach(function(keyInteresse) {
              var row = result[key][keyInteresse];
              interessen[keyInteresse] = { name: row.Fachgebiet, id: row.FachgebietId };
            });
            break;
          case '2':
            Object.keys(result[key]).forEach(function(keySchule) {
              var row = result[key][keySchule];
              schule[keySchule] = { name: row.Schule, id: row.SchuleId };
            });
            break;
          case '3':
            Object.keys(result[key]).forEach(function(keyAngebote) {
              var row = result[key][keyAngebote];
              angebote[keyAngebote] = { name: row.Angebot, id: row.AngebotId };
            });
            break;
          case '4':
            Object.keys(result[key]).forEach(function(keyLand) {
              var row = result[key][keyLand];
              länder[row.LandId] = { land: row.LongName, id: row.LandId };
              länder[row.LandId].bundeslander = [];
            });
            break;
          case '5':
            Object.keys(result[key]).forEach(function(keySchultyp) {
              var row = result[key][keySchultyp];
              schultypen[keySchultyp] = { name: row.Ausbildung, id: row.AusbildungId };
            });
            break;
          case '6':
            Object.keys(result[key]).forEach(function(keyGeschlecht) {
              var row = result[key][keyGeschlecht];
              geschlechter[keyGeschlecht] = { name: row.Geschlecht, id: row.GeschlechtId };
            });
            break;
          case '7':
            Object.keys(result[key]).forEach(function(keyCampus) {
              var row = result[key][keyCampus];
              campus[row.CampusId] = { name: row.Campus, id: row.CampusId };
              campus[row.CampusId].bachelor = [];
              campus[row.CampusId].master = [];
            });
            break;
          case '8':
            Object.keys(result[key]).forEach(function(keyBundesland) {
              var row = result[key][keyBundesland];
              if(row.LandId != null){
                länder[row.LandId].bundeslander.push({ name: row.Bundesland, id: row.BundeslandId });
              }
            });
            break;
          case '9':
            Object.keys(result[key]).forEach(function(keyStudiengang) {
              var row = result[key][keyStudiengang];
              if(row.Abschluss[0] == 'B'){
                campus[row.CampusId].bachelor.push({ name: row.Studiengang, id: row.StudiengangId });
              } else {
                campus[row.CampusId].master.push({ name: row.Studiengang, id: row.StudiengangId });
              }
              
            });
            break;
        }
      });
      Object.keys(länder).forEach(function(keyland) {
        länder[keyland].bundeslander.push({ name: "Anderes Land", id: 10 });
      });
      
      interessen = Object.values(interessen);
      angebote = Object.values(angebote);
      schule = Object.values(schule);
      campus = Object.values(campus);
      länder = Object.values(länder);
      schultypen = Object.values(schultypen);
      geschlechter = Object.values(geschlechter);
      if(von == 0){
        res.json({
          "Errorcode": 1
        });
      } else {
        res.json({
          "Errorcode": 0,
          "Passwort": req.query.passwort,
          "Beginn" : von,
          "Ende" : bis,
          "Schultyp": 
            schultypen
          ,
          "Land": 
            länder 
          ,
          "Schulen":
            schule
          ,
          "Geschlecht":
            geschlechter
          ,
          "Campus": 
             campus    
          ,
          "Angebote": 
            angebote
          ,
          "Interessen":
            interessen
          ,
          "Beraterid": beraterId,
          "Veranstaltungid": veranstaltungId,
          "Briefing": briefing,
          "Token": "sfjlaskfn43743hafajsdfjlsadhfow"
        });
      }
    });
   });

app.get("/api/logout", (req, res, next) => {
res.json({
    "passwort": req.query.passwort,
    "token": "sfjlaskfn43743hafajsdfjlsadhfow"
    });
});

app.get("/api/termine", (req, res, next) => {
    var query_id = req.query.id;
    database.getTermineById(query_id, function(error, result) {
        return res.json(result);
      });
    });

app.post("/api/beratung", (req, res, next) => {
  body = req.body;
  Object.keys(body.SessionList).forEach(function(key) {
    sess = body.SessionList[key];
    console.log(body.VeranstaltungsId);
    database.createBeratung(body.VeranstaltungsId, function(error, result) {
      var id = result[0][0].BeratungsId;
      //Object.keys(body.beratungen[key].session).forEach(function(keySession) {
      database.createSession('2020-09-27 11:51:47', '2020-09-30 08:35:52', sess.Bewertung,1, body.BeraterId, id, function(error, result) {
        var sessionid = result[0][0].SessionId;
      });
      //});
    });
  });
  res.json({
    "resultCode": 1
    });
});


module.exports = app;

var port = process.env.PORT || 3000;

app.listen(port, function () {
  console.log('Server running at http://127.0.0.1:' + port + '/');
});