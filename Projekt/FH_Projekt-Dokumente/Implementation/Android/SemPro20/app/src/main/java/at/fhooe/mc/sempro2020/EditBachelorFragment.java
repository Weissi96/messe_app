package at.fhooe.mc.sempro2020;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * This fragment shows the bachelor types.
 */
public class EditBachelorFragment extends Fragment {
    // The TAG is there to tag the fragment.
    private static final String TAG = "EditBachelorFragment";

    //
    boolean[] mBacArray = new boolean[8];

    // Default constructor.
    public EditBachelorFragment() {
    }

    /**
     * The constructor which inititializes the fragment.
     *
     * @return the initialized frament
     */
    public static EditBachelorFragment newInstance() {
        EditBachelorFragment fragment = new EditBachelorFragment();
        return fragment;
    }

    /**
     * This method initializes the fragment.
     *
     * @param savedInstanceState saves the current state of the activity
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Here the view and the whole layout objects are getting initialized.
     *
     * @param _inflater           initializes the layout file into the view
     * @param _container          where the view is inside
     * @param _savedInstanceState saves the current state of the activity
     * @return the view
     */
    @Override
    public View onCreateView(LayoutInflater _inflater, ViewGroup _container,
                             Bundle _savedInstanceState) {
        final View view = _inflater.inflate(R.layout.fragment_edit_bachelor, _container, false);

        final ImageView acCircle = view.findViewById(R.id.fm_edit_bachelor_ac_circle);
        acCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "EditCampusFragment: onClick() hsd-checkbox");
                ImageView circleNew = view.findViewById(R.id.fm_edit_bachelor_ac_circle_filled);
                if (!mBacArray[0]) {
                    circleNew.setVisibility(_v.VISIBLE);
                    mBacArray[0] = true;
                } else {
                    circleNew.setVisibility(_v.INVISIBLE);
                    mBacArray[0] = false;
                }
            }
        });

        final ImageView hsdCircle = view.findViewById(R.id.fm_edit_bachelor_hsd_circle);
        hsdCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "EditCampusFragment: onClick() hsd-checkbox");
                ImageView circleNew = view.findViewById(R.id.fm_edit_bachelor_hsd_circle_filled);
                if (!mBacArray[1]) {
                    circleNew.setVisibility(_v.VISIBLE);
                    mBacArray[1] = true;
                } else {
                    circleNew.setVisibility(_v.INVISIBLE);
                    mBacArray[1] = false;
                }
            }
        });

        final ImageView kwmCircle = view.findViewById(R.id.fm_edit_bachelor_kwm_circle);
        kwmCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "EditCampusFragment: onClick() kwm-checkbox");
                ImageView circleNew = view.findViewById(R.id.fm_edit_bachelor_kwm_circle_filled);
                if (!mBacArray[2]) {
                    circleNew.setVisibility(_v.VISIBLE);
                    mBacArray[2] = true;
                } else {
                    circleNew.setVisibility(_v.INVISIBLE);
                    mBacArray[2] = false;
                }
            }
        });

        final ImageView mtdCircle = view.findViewById(R.id.fm_edit_bachelor_mtd_circle);
        mtdCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "EditCampusFragment: onClick() hagenberg-checkbox");
                ImageView circleNew = view.findViewById(R.id.fm_edit_bachelor_mtd_circle_filled);
                if (!mBacArray[3]) {
                    circleNew.setVisibility(_v.VISIBLE);
                    mBacArray[3] = true;
                } else {
                    circleNew.setVisibility(_v.INVISIBLE);
                    mBacArray[3] = false;
                }
            }
        });

        final ImageView mbiCircle = view.findViewById(R.id.fm_edit_bachelor_mbi_circle);
        mbiCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "EditCampusFragment: onClick() mbi-checkbox");
                ImageView circleNew = view.findViewById(R.id.fm_edit_bachelor_mbi_circle_filled);
                if (!mBacArray[4]) {
                    circleNew.setVisibility(_v.VISIBLE);
                    mBacArray[4] = true;
                } else {
                    circleNew.setVisibility(_v.INVISIBLE);
                    mBacArray[4] = false;
                }
            }
        });

        final ImageView mcCircle = view.findViewById(R.id.fm_edit_bachelor_mc_circle);
        mcCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "EditCampusFragment: onClick() mc-checkbox");
                ImageView circleNew = view.findViewById(R.id.fm_edit_bachelor_mc_circle_filled);
                if (!mBacArray[5]) {
                    circleNew.setVisibility(_v.VISIBLE);
                    mBacArray[5] = true;
                } else {
                    circleNew.setVisibility(_v.INVISIBLE);
                    mBacArray[5] = false;
                }
            }
        });

        final ImageView siCircle = view.findViewById(R.id.fm_edit_bachelor_si_circle);
        siCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "EditCampusFragment: onClick() si-checkbox");
                ImageView circleNew = view.findViewById(R.id.fm_edit_bachelor_si_circle_filled);
                if (!mBacArray[6]) {
                    circleNew.setVisibility(_v.VISIBLE);
                    mBacArray[6] = true;
                } else {
                    circleNew.setVisibility(_v.INVISIBLE);
                    mBacArray[6] = false;
                }
            }
        });

        final ImageView seCircle = view.findViewById(R.id.fm_edit_bachelor_se_circle);
        seCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "EditCampusFragment: onClick() se-checkbox");
                ImageView circleNew = view.findViewById(R.id.fm_edit_bachelor_se_circle_filled);
                if (!mBacArray[7]) {
                    circleNew.setVisibility(_v.VISIBLE);
                    mBacArray[7] = true;
                } else {
                    circleNew.setVisibility(_v.INVISIBLE);
                    mBacArray[7] = false;
                }
            }
        });

        return view;
    }
}
