package at.fhooe.mc.sempro2020;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

/**
 * In this class the user can log out of the system.
 */
public class LogoutActivity extends AppCompatActivity {
    // The TAG is there to tag the activity.
    private static final String TAG = "LogoutActivity";

    /**
     * The whole layout objects are initialized and the logout is getting done.
     *
     * @param _savedInstanceState saves the current state of the activity
     */
    @Override
    protected void onCreate(Bundle _savedInstanceState) {
        super.onCreate(_savedInstanceState);
        setContentView(R.layout.activity_logout);

        Button checkButton = findViewById(R.id.activity_logout_button);
        checkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                moveTaskToBack(true);
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
            }
        });

        ImageView backButton = findViewById(R.id.activity_logout_back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "MainActivity: onClick() Button");
                Intent i = new Intent(LogoutActivity.this, MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });
    }
}
