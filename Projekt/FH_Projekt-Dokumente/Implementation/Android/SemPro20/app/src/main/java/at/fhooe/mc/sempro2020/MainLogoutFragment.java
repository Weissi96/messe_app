package at.fhooe.mc.sempro2020;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

/**
 * This class provides the fragment of the logout which is shown in the timeline.
 */
public class MainLogoutFragment extends Fragment {
    // The TAG is there to tag the fragment.
    private static final String TAG = "MainLogoutFragment";

    /**
     * Default Constructor.
     */
    public MainLogoutFragment() {
    }

    /**
     * The constructor which initializes the fragment.
     *
     * @return the initialized frament
     */
    public static MainLogoutFragment newInstance() {
        MainLogoutFragment fragment = new MainLogoutFragment();
        return fragment;
    }


    /**
     * This method initializes the fragment.
     *
     * @param _savedInstanceState saves the current state of the activity
     */
    @Override
    public void onCreate(@Nullable Bundle _savedInstanceState) {
        super.onCreate(_savedInstanceState);
    }

    /**
     * Here the view and the whole layout objects are getting initialized.
     *
     * @param _inflater           initializes the layout file into the view
     * @param _container          where the view is inside
     * @param _savedInstanceState saves the current state of the activity
     * @return the view
     */
    @Override
    public View onCreateView(LayoutInflater _inflater, ViewGroup _container,
                             Bundle _savedInstanceState) {
        final View view = _inflater.inflate(R.layout.fragment_main_logout, _container, false);

        ImageView logout = view.findViewById(R.id.fm_main_logout_background);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "LogoutActivity: onClick() Button");
                Intent i = new Intent(getContext(), LogoutActivity.class);
                startActivity(i);
            }
        });

        logout = view.findViewById(R.id.fm_main_logout_background_colored);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "LogoutActivity: onClick() Button");
                Intent i = new Intent(getContext(), LogoutActivity.class);
                startActivity(i);
            }
        });

        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) view.getLayoutParams();
        params.height = 600;

        return view;
    }

}
