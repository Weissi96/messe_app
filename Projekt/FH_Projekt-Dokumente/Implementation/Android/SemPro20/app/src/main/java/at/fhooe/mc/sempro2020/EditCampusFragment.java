package at.fhooe.mc.sempro2020;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * This fragment shows the campuses.
 */
public class EditCampusFragment extends Fragment {
    // The TAG is there to tag the fragment.
    private static final String TAG = "EditCampusFragment";

    // Is true if it is selected.
    public boolean mHagenberg;

    // Is true if it is selected.
    public boolean mLinz;

    // Is true if it is selected.
    public boolean mSteyr;

    // Is true if it is selected.
    public boolean mWels;

    // Is the key to save the right value to the shared preferences.
    public static String HGB_KEY = String.valueOf(R.string.campus_hgb_key);

    // Is the key to save the right value to the shared preferences.
    public static String LNZ_KEY = String.valueOf(R.string.campus_lnz_key);

    // Is the key to save the right value to the shared preferences.
    public static String STR_KEY = String.valueOf(R.string.campus_str_key);

    // Is the key to save the right value to the shared preferences.
    public static String WLS_KEY = String.valueOf(R.string.campus_wls_key);

    // The shared preferences are used to save the data locally.
    SharedPreferences sp;

    /**
     * Default constructor.
     */
    public EditCampusFragment() {
    }

    /**
     * The constructor which inititializes the fragment.
     *
     * @return the initialized frament
     */
    public static EditCampusFragment newInstance() {
        EditCampusFragment fragment = new EditCampusFragment();
        return fragment;
    }

    /**
     * This method initializes the fragment.
     *
     * @param savedInstanceState saves the current state of the activity
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Here the view and the whole layout objects are getting initialized.
     *
     * @param _inflater           initializes the layout file into the view
     * @param _container          where the view is inside
     * @param _savedInstanceState saves the current state of the activity
     * @return the view
     */
    @Override
    public View onCreateView(LayoutInflater _inflater, ViewGroup _container,
                             Bundle _savedInstanceState) {

        final View view = _inflater.inflate(R.layout.fragment_edit_campus, _container, false);

        sp = BeratungAdapter.mContext.getSharedPreferences(BeratungAdapter.PREF_KEY, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sp.edit();
        editor.clear().commit();

        final ImageView hagenbergCircle = view.findViewById(R.id.fm_edit_campus_hagenberg_circle);
        hagenbergCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "EditCampusFragment: onClick() hagenberg-checkbox");
                ImageView circleNew = view.findViewById(R.id.fm_edit_campus_hagenberg_circle_filled);
                if (!mHagenberg) {
                    circleNew.setVisibility(_v.VISIBLE);
                    editor.putString(HGB_KEY, "Hagenberg");
                    editor.apply();
                    mHagenberg = true;
                } else {
                    circleNew.setVisibility(_v.INVISIBLE);
                    editor.remove(HGB_KEY);
                    editor.apply();
                    mHagenberg = false;
                }
            }
        });

        final ImageView linzCircle = view.findViewById(R.id.fm_edit_campus_linz_circle);
        linzCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "EditCampusFragment: onClick() linz-checkbox");
                ImageView circleNew = view.findViewById(R.id.fm_edit_campus_linz_circle_filled);
                if (!mLinz) {
                    circleNew.setVisibility(_v.VISIBLE);
                    editor.putString(LNZ_KEY, "Linz");
                    editor.apply();
                    mLinz = true;
                } else {
                    circleNew.setVisibility(_v.INVISIBLE);
                    editor.remove(LNZ_KEY);
                    editor.apply();
                    mLinz = false;
                }
            }
        });

        final ImageView steyrCircle = view.findViewById(R.id.fm_edit_campus_steyr_circle);
        steyrCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "EditCampusFragment: onClick() steyr-checkbox");
                ImageView circleNew = view.findViewById(R.id.fm_edit_campus_steyr_circle_filled);
                if (!mSteyr) {
                    circleNew.setVisibility(_v.VISIBLE);
                    editor.putString(STR_KEY, "Steyr");
                    editor.apply();
                    mSteyr = true;
                } else {
                    circleNew.setVisibility(_v.INVISIBLE);
                    editor.remove(STR_KEY);
                    editor.apply();
                    mSteyr = false;
                }
            }
        });

        final ImageView welsCircle = view.findViewById(R.id.fm_edit_campus_wels_circle);
        welsCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "EditCampusFragment: onClick() wels-checkbox");
                ImageView circleNew = view.findViewById(R.id.fm_edit_campus_wels_circle_filled);
                if (!mWels) {
                    circleNew.setVisibility(_v.VISIBLE);
                    editor.putString(WLS_KEY, "Wels");
                    editor.apply();
                    mWels = true;
                } else {
                    circleNew.setVisibility(_v.INVISIBLE);
                    editor.remove(WLS_KEY);
                    editor.apply();
                    mWels = false;
                }
            }
        });

        return view;
    }
}
