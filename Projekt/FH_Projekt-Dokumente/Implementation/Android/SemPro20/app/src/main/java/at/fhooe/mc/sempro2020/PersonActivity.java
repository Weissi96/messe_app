package at.fhooe.mc.sempro2020;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

/**
 * This class provides all the important information about the person.
 */
public class PersonActivity extends AppCompatActivity {
    // The TAG is there to tag the activity.
    private static final String TAG = "PersonActivity";

    // Is the String which is selected in the spinner.
    String geschlecht;

    // Is the String which is selected in the spinner.
    String schultyp;

    // Is the String which is selected in the spinner.
    String bundesland;

    // Is the String which is selected in the spinner.
    String schule;

    // The shared preferences are used to save the data locally.
    SharedPreferences sp;

    // Is a spinner with values about the person.
    Spinner spin;

    // Is the key to save the right value to the shared preferences.
    public static String GESCHLECHT_KEY = String.valueOf(R.string.person_geschlecht_key);

    // Is the key to save the right value to the shared preferences.
    public static String BUNDESLAND_KEY = String.valueOf(R.string.person_bundesland_key);

    // Is the key to save the right value to the shared preferences.
    public static String SCHULTYP_KEY = String.valueOf(R.string.person_schultyp_key);

    // Is the key to save the right value to the shared preferences.
    public static String SCHULE_KEY = String.valueOf(R.string.person_schule_key);

    /**
     * The whole layout objects are initialized.
     *
     * @param _savedInstanceState saves the current state of the activity
     */
    @Override
    protected void onCreate(Bundle _savedInstanceState) {
        super.onCreate(_savedInstanceState);
        setContentView(R.layout.activity_person);

        sp = getSharedPreferences(BeratungAdapter.PREF_KEY, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sp.edit();
        editor.clear().commit();

        spin = findViewById(R.id.activity_person_spinner);

        ArrayAdapter<CharSequence> myAdapter = ArrayAdapter.createFromResource(this, R.array.person_geschlecht_array, R.layout.person_spinner_item);
        myAdapter.setDropDownViewResource(R.layout.person_spinner_entries);
        spin.setAdapter(myAdapter);
        spin.setSelection(2);
        geschlecht = spin.getSelectedItem().toString();
        editor.putString(GESCHLECHT_KEY, geschlecht);

        spin = findViewById(R.id.activity_person_spinner1);

        myAdapter = ArrayAdapter.createFromResource(this, R.array.person_schultyp_array, R.layout.person_spinner_item);
        myAdapter.setDropDownViewResource(R.layout.person_spinner_entries);
        spin.setAdapter(myAdapter);
        spin.setSelection(4);
        schultyp = spin.getSelectedItem().toString();
        editor.putString(SCHULTYP_KEY, schultyp);

        spin = findViewById(R.id.activity_person_spinner2);

        myAdapter = ArrayAdapter.createFromResource(this, R.array.person_bundesland_array, R.layout.person_spinner_item);
        myAdapter.setDropDownViewResource(R.layout.person_spinner_entries);
        spin.setAdapter(myAdapter);
        spin.setSelection(5);
        bundesland = spin.getSelectedItem().toString();
        editor.putString(BUNDESLAND_KEY, bundesland);

        spin = findViewById(R.id.activity_person_spinner3);

        myAdapter = ArrayAdapter.createFromResource(this, R.array.person_schule_array, R.layout.person_spinner_item);
        myAdapter.setDropDownViewResource(R.layout.person_spinner_entries);
        spin.setAdapter(myAdapter);
        spin.setSelection(0);
        schule = spin.getSelectedItem().toString();
        editor.putString(SCHULE_KEY, schule);

        Button checkButton = findViewById(R.id.activity_person_button);
        checkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                editor.apply();
                Log.i(TAG, "BeratungActivity: onClick() Button");
                Intent i = new Intent(PersonActivity.this, BeratungActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });

        ImageView backButton = findViewById(R.id.activity_person_back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "BeratungActivity: onClick() Button");
                Intent i = new Intent(PersonActivity.this, BeratungActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });
    }
}
