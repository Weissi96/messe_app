package at.fhooe.mc.sempro2020;

import java.util.List;

/**
 * This class provides a Session-object. It consists of a start and end time and of lists about
 * Campus, Angebot, Bachelor, Master. It has one Person object and a feedback. Sessions are
 * inside of consultations.
 */
class Session {
    // This int is the start time as a long.
    long Start;

    // This int is the end time as a long.
    long End;

    // This is a list of "Campus"-Strings.
    List<String> CampusList;

    // This is a list of "Angebot"-Strings.
    List<String> AngebotList;

    // This is a list of "Bachelor"-Strings.
    List<String> BachelorList;

    // This is a list of "Master"-Strings.
    List<String> MasterList;

    // This is a list of "Interessen"-Strings.
    List<String> InteressenList;

    // This is the interviewed person.
    Person Person;

    // This is the feedback the user gives the interview.
    int Bewerung;

    /**
     * This constructor initializes all parameters.
     *
     * @param _start          is the start time
     * @param _end            is the end time
     * @param _campusList     is a list of "Campus"-Strings
     * @param _angebotList    is a list of "Angebot"-Strings
     * @param _bachelorList   is a list of "Bachelor"-Strings
     * @param _masterList     is a list of "Master"-Strings
     * @param _interessenList is a list of "Interessen"-Strings
     * @param _person         is the interviewed person
     * @param _bewerung       is the feedback
     */
    public Session(long _start, long _end, List<String> _campusList, List<String> _angebotList, List<String> _bachelorList,
                   List<String> _masterList, List<String> _interessenList, Person _person, int _bewerung) {
        Start = _start;
        End = _end;
        CampusList = _campusList;
        AngebotList = _angebotList;
        BachelorList = _bachelorList;
        MasterList = _masterList;
        InteressenList = _interessenList;
        Person = _person;
        Bewerung = _bewerung;
    }

    /**
     * Default constructor.
     */
    public Session() {
        Start = -1;
        End = -1;
        CampusList = null;
        AngebotList = null;
        BachelorList = null;
        MasterList = null;
        Person = null;
        Bewerung = -1;
    }
}
