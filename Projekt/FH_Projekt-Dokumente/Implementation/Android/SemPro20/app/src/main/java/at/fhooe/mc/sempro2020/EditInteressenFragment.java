package at.fhooe.mc.sempro2020;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * This fragment shows the interessts.
 */
public class EditInteressenFragment extends Fragment {
    // The TAG is there to tag the fragment.
    private static final String TAG = "EditInteressenFragment";

    // Is true if it is selected.
    boolean mEnglisch;

    // Is true if it is selected.
    boolean mInfo;

    // Is true if it is selected.
    boolean mLife;

    // Is true if it is selected.
    boolean mMedien;

    // Is true if it is selected.
    boolean mSozial;

    // Is true if it is selected.
    boolean mTechnik;

    // Is true if it is selected.
    boolean mUmwelt;

    // Is true if it is selected.
    boolean mWM;

    // Is true if it is selected.
    boolean mWT;

    // Is the key to save the right value to the shared preferences.
    public static String ESTUDIEN_KEY = String.valueOf(R.string.int_englisch_key);

    // Is the key to save the right value to the shared preferences.
    public static String INFORMATIK_KEY = String.valueOf(R.string.int_informatik_key);

    // Is the key to save the right value to the shared preferences.
    public static String LIFESCIENCES_KEY = String.valueOf(R.string.int_life_key);

    // Is the key to save the right value to the shared preferences.
    public static String MEDIENKOMM_KEY = String.valueOf(R.string.int_medien_key);

    // Is the key to save the right value to the shared preferences.
    public static String SOZIALES_KEY = String.valueOf(R.string.int_soziales_key);

    // Is the key to save the right value to the shared preferences.
    public static String TECHNIK_KEY = String.valueOf(R.string.int_technik_key);

    // Is the key to save the right value to the shared preferences.
    public static String UMWELT_KEY = String.valueOf(R.string.int_umwelt_key);

    // Is the key to save the right value to the shared preferences.
    public static String WIRTMANAGE_KEY = String.valueOf(R.string.int_wm_key);

    // Is the key to save the right value to the shared preferences.
    public static String WIRTTECH_KEY = String.valueOf(R.string.int_wt_key);

    // The shared preferences are used to save the data locally.
    SharedPreferences sp;

    /**
     * Default constructor.
     */
    public EditInteressenFragment() {
    }


    /**
     * The constructor which initializes the fragment.
     *
     * @return the initialized frament
     */
    public static EditInteressenFragment newInstance() {
        EditInteressenFragment fragment = new EditInteressenFragment();
        return fragment;
    }

    /**
     * This method initializes the fragment.
     *
     * @param _savedInstanceState saves the current state of the activity
     */
    @Override
    public void onCreate(Bundle _savedInstanceState) {
        super.onCreate(_savedInstanceState);
    }

    /**
     * Here the view and the whole layout objects are getting initialized.
     *
     * @param _inflater           initializes the layout file into the view
     * @param _container          where the view is inside
     * @param _savedInstanceState saves the current state of the activity
     * @return the view
     */
    @Override
    public View onCreateView(LayoutInflater _inflater, ViewGroup _container,
                             Bundle _savedInstanceState) {
        final View view = _inflater.inflate(R.layout.fragment_edit_interessen, _container, false);

        sp = BeratungAdapter.mContext.getSharedPreferences(BeratungAdapter.PREF_KEY, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sp.edit();
        editor.clear().commit();

        final ImageView englischCircle = view.findViewById(R.id.fm_edit_interessen_englisch_circle);
        englischCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "EditInteressenFragment: onClick() englisch-checkbox");
                ImageView circleNew = view.findViewById(R.id.fm_edit_interessen_englisch_circle_filled);
                if (!mEnglisch) {
                    circleNew.setVisibility(_v.VISIBLE);
                    editor.putString(ESTUDIEN_KEY, "Englischsprachige Studien");
                    editor.apply();
                    mEnglisch = true;
                } else {
                    circleNew.setVisibility(_v.INVISIBLE);
                    editor.remove(ESTUDIEN_KEY);
                    editor.apply();
                    mEnglisch = false;
                }
            }
        });

        final ImageView infoCircle = view.findViewById(R.id.fm_edit_interessen_info_circle);
        infoCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "EditInteressenFragment: onClick() informatik-checkbox");
                ImageView circleNew = view.findViewById(R.id.fm_edit_interessen_info_circle_filled);
                if (!mInfo) {
                    circleNew.setVisibility(_v.VISIBLE);
                    editor.putString(INFORMATIK_KEY, "Informatik");
                    editor.apply();
                    mInfo = true;
                } else {
                    circleNew.setVisibility(_v.INVISIBLE);
                    editor.remove(INFORMATIK_KEY);
                    editor.apply();
                    mInfo = false;
                }
            }
        });

        final ImageView lifeCircle = view.findViewById(R.id.fm_edit_interessen_life_circle);
        lifeCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "EditInteressenFragment: onClick() life sciences-checkbox");
                ImageView circleNew = view.findViewById(R.id.fm_edit_interessen_life_circle_filled);
                if (!mLife) {
                    circleNew.setVisibility(_v.VISIBLE);
                    editor.putString(LIFESCIENCES_KEY, "Life Sciences");
                    editor.apply();
                    mLife = true;
                } else {
                    circleNew.setVisibility(_v.INVISIBLE);
                    editor.remove(LIFESCIENCES_KEY);
                    editor.apply();
                    mLife = false;
                }
            }
        });

        final ImageView medienCircle = view.findViewById(R.id.fm_edit_interessen_medien_circle);
        medienCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "EditInteressenFragment: onClick() medien-checkbox");
                ImageView circleNew = view.findViewById(R.id.fm_edit_interessen_medien_circle_filled);
                if (!mMedien) {
                    circleNew.setVisibility(_v.VISIBLE);
                    editor.putString(MEDIENKOMM_KEY, "Medien & Kommunikation");
                    editor.apply();
                    mMedien = true;
                } else {
                    circleNew.setVisibility(_v.INVISIBLE);
                    editor.remove(MEDIENKOMM_KEY);
                    editor.apply();
                    mMedien = false;
                }
            }
        });

        final ImageView sozialCircle = view.findViewById(R.id.fm_edit_interessen_soziales_circle);
        sozialCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "EditInteressenFragment: onClick() soziales-checkbox");
                ImageView circleNew = view.findViewById(R.id.fm_edit_interessen_soziales_circle_filled);
                if (!mSozial) {
                    circleNew.setVisibility(_v.VISIBLE);
                    editor.putString(SOZIALES_KEY, "Soziales");
                    editor.apply();
                    mSozial = true;
                } else {
                    circleNew.setVisibility(_v.INVISIBLE);
                    editor.remove(SOZIALES_KEY);
                    editor.apply();
                    mSozial = false;
                }
            }
        });

        final ImageView technikCircle = view.findViewById(R.id.fm_edit_interessen_technik_circle);
        technikCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "EditInteressenFragment: onClick() technik-checkbox");
                ImageView circleNew = view.findViewById(R.id.fm_edit_interessen_technik_circle_filled);
                if (!mTechnik) {
                    circleNew.setVisibility(_v.VISIBLE);
                    editor.putString(TECHNIK_KEY, "Technik");
                    editor.apply();
                    mTechnik = true;
                } else {
                    circleNew.setVisibility(_v.INVISIBLE);
                    editor.remove(TECHNIK_KEY);
                    editor.apply();
                    mTechnik = false;
                }
            }
        });

        final ImageView umweltCircle = view.findViewById(R.id.fm_edit_interessen_umwelt_circle);
        umweltCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "EditInteressenFragment: onClick() umwelt-checkbox");
                ImageView circleNew = view.findViewById(R.id.fm_edit_interessen_umwelt_circle_filled);
                if (!mUmwelt) {
                    circleNew.setVisibility(_v.VISIBLE);
                    editor.putString(UMWELT_KEY, "Umwelt & Energie");
                    editor.apply();
                    mUmwelt = true;
                } else {
                    circleNew.setVisibility(_v.INVISIBLE);
                    editor.remove(UMWELT_KEY);
                    editor.apply();
                    mUmwelt = false;
                }
            }
        });

        final ImageView wmCircle = view.findViewById(R.id.fm_edit_interessen_wm_circle);
        wmCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "EditInteressenFragment: onClick() wirtschaft management-checkbox");
                ImageView circleNew = view.findViewById(R.id.fm_edit_interessen_wm_circle_filled);
                if (!mWM) {
                    circleNew.setVisibility(_v.VISIBLE);
                    editor.putString(WIRTMANAGE_KEY, "Wirtschaft & Management");
                    editor.apply();
                    mWM = true;
                } else {
                    circleNew.setVisibility(_v.INVISIBLE);
                    editor.remove(WIRTMANAGE_KEY);
                    editor.apply();
                    mWM = false;
                }
            }
        });

        final ImageView wtCircle = view.findViewById(R.id.fm_edit_interessen_wt_circle);
        wtCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "EditInteressenFragment: onClick() wirtschaft technik-checkbox");
                ImageView circleNew = view.findViewById(R.id.fm_edit_interessen_wt_circle_filled);
                if (!mWT) {
                    circleNew.setVisibility(_v.VISIBLE);
                    editor.putString(WIRTTECH_KEY, "Wirtschaft & Technik");
                    editor.apply();
                    mWT = true;
                } else {
                    circleNew.setVisibility(_v.INVISIBLE);
                    editor.remove(WIRTTECH_KEY);
                    editor.apply();
                    mWT = false;
                }
            }
        });

        return view;
    }
}
