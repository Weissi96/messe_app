package at.fhooe.mc.sempro2020;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.JsonReader;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * This class provides the main activity where all actions start. A timeline with all
 * consultations is shown.
 */
public class MainActivity extends AppCompatActivity {
    // The TAG is there to tag the activity.
    private static final String TAG = "MainActivity";

    // Is the login fragment on the timeline.
    MainLoginFragment mFmlogin;

    // Is the logout fragment on the timeline.
    MainLogoutFragment mFmlogout;

    // Is the consultation fragment on the timeline. There are many ot these.
    MainFragment mFragment;

    // Is true if the user already logged in.
    private boolean mLogin;

    /**
     * The whole layout objects are initialized. When the login code is right, all components
     * will be visible.
     *
     * @param _savedInstanceState saves the current state of the activity
     */
    @Override
    protected void onCreate(Bundle _savedInstanceState) {
        super.onCreate(_savedInstanceState);
        setContentView(R.layout.activity_main);

        if (!mLogin) {
            mFmlogin = new MainLoginFragment();
            activateFragment(mFmlogin);
            mLogin = true;
        }

        ImageView addPerson = findViewById(R.id.activity_beratung_add_person);

        if (LoginActivity.mRightCode && !mFmlogin.mTimeCheck) {
            new GetData().execute();

            mFragment = new MainFragment(1);
            activateFragment(mFragment);

            mFragment = new MainFragment(2);
            activateFragment(mFragment);

            mFragment = new MainFragment(3);
            activateFragment(mFragment);

            mFragment = new MainFragment(4);
            activateFragment(mFragment);

            mFmlogout = new MainLogoutFragment();
            activateFragment(mFmlogout);

            addPerson.setVisibility(View.VISIBLE);
            addPerson.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View _v) {
                    Log.i(TAG, "CameraActivity: onClick() Button");
                    Intent i = new Intent(MainActivity.this, CameraActivity.class)
                            .putExtra("FROM_ACTIVITY", "MAIN")
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }
            });
        }
    }

    /**
     * This method activates the fragment.
     *
     * @param _fm is the given fragment
     */
    private void activateFragment(Fragment _fm) {
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.activity_main_framelayout, _fm)
                .commit();
    }

    /**
     * This method deletes the fragment.
     *
     * @param _fm is the given fragment
     */
    private void removeFragment(Fragment _fm) {
        getSupportFragmentManager()
                .beginTransaction()
                .remove(_fm)
                .commit();
    }

    /**
     *
     */
    @SuppressLint("StaticFieldLeak")
    private class GetData extends AsyncTask<Void, Void, Void> {

        /**
         *
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Toast.makeText(MainActivity.this, "Json Data is downloading..",
                    Toast.LENGTH_LONG).show();

        }

        /**
         * @param _voids
         * @return
         */
        @Override
        protected Void doInBackground(Void... _voids) {
            HttpHandler handler = new HttpHandler();
            String url = "https://messeapp.herokuapp.com/api/login?passwort=123";
            String json = handler.makeServiceCall(url);
            Log.e(TAG, "Response from url: " + json);

            if (json != null) {
                try {
                    JSONObject jsonObj = new JSONObject(json);
                    String key;
                    Set<String> data = new HashSet<>();

                    SharedPreferences pref = getApplicationContext().getSharedPreferences("ServerData", 0);
                    @SuppressLint("CommitPrefEdits") SharedPreferences.Editor editor = pref.edit();

                    String password = jsonObj.getString("passwort");
                    String begin = jsonObj.getString("beginn");
                    String duration = jsonObj.getString("dauer");
                    String end = jsonObj.getString("ende");

                    editor.putString("password", password);
                    editor.putString("begin", begin);
                    editor.putString("duration", duration);
                    editor.putString("end", end);

                    JSONArray schooltype = jsonObj.getJSONArray("schultyp");
                    for (int j = 0; j < schooltype.length(); j++) data.add(schooltype.getString(j));
                    editor.putStringSet("schooltype", data);
                    data.clear();

                    JSONArray district = jsonObj.getJSONArray("bundesland");
                    for (int j = 0; j < district.length(); j++) data.add(district.getString(j));
                    editor.putStringSet("district", data);
                    data.clear();

                    JSONArray school = jsonObj.getJSONArray("schulen");
                    for (int j = 0; j < school.length(); j++) data.add(school.getString(j));
                    editor.putStringSet("school", data);
                    data.clear();

                    JSONArray gender = jsonObj.getJSONArray("geschlecht");
                    for (int j = 0; j < gender.length(); j++) data.add(gender.getString(j));
                    editor.putStringSet("gender", data);
                    data.clear();

                    JSONArray campus = jsonObj.getJSONArray("campus");
                    for (int j = 0; j < campus.length(); j++) data.add(campus.getString(j));
                    editor.putStringSet("campus", data);
                    data.clear();

                    JSONArray course = jsonObj.getJSONArray("studiengang");
                    for (int j = 0; j < course.length(); j++) data.add(course.getString(j));
                    editor.putStringSet("course", data);
                    data.clear();

                    JSONArray offer = jsonObj.getJSONArray("angebote");
                    for (int j = 0; j < offer.length(); j++) data.add(offer.getString(j));
                    editor.putStringSet("offer", data);
                    data.clear();

                    String userId = jsonObj.getString("beraterid");
                    String briefing = jsonObj.getString("briefing");

                    editor.putString("userId", userId);
                    editor.putString("briefing", briefing);

                    JSONArray date = jsonObj.getJSONArray("termine");
                    for (int j = 0; j < date.length(); j++) data.add(date.getString(j));
                    editor.putStringSet("date", data);
                    data.clear();

                    String token = jsonObj.getString("token");

                    editor.putString("token", token);

                    editor.apply();

                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                    });

                }

            } else {
                Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG).show();
                    }
                });
            }

            return null;
        }
    }
}

