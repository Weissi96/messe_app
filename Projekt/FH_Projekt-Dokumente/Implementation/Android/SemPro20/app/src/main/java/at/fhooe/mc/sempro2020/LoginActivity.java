package at.fhooe.mc.sempro2020;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * This class provides login to the system with a given code.
 */
public class LoginActivity extends AppCompatActivity {
     // The TAG is there to tag the activity.
    private static final String TAG = "LoginActivity";

     // Is true, if the code was right.
    public static boolean mRightCode = false;

     // The EditText field is for the user to tip in the code.
    private EditText mCode;


    /**
     * The whole layout objects are initialized and the login code is getting detected.
     *
     * @param _savedInstanceState saves the current state of the activity
     */
    @Override
    protected void onCreate(Bundle _savedInstanceState) {
        super.onCreate(_savedInstanceState);
        setContentView(R.layout.activity_login);
        mCode = findViewById(R.id.activity_login_code);

        Button checkButton = findViewById(R.id.activity_login_button);
        checkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                if (mCode.getText().toString().equals("123")) {
                    mRightCode = true;
                    Log.i(TAG, "MainActivity: onClick() Button");
                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                } else {
                    Toast toast = Toast.makeText(LoginActivity.this, "Wrong code!", Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        });

        ImageView backButton = findViewById(R.id.activity_login_back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "MainActivity: onClick() Button");
                Intent i = new Intent(LoginActivity.this, MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });
    }
}
