package at.fhooe.mc.sempro2020;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetector;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetectorOptions;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.common.FirebaseVisionImageMetadata;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.otaliastudios.cameraview.CameraView;
import com.otaliastudios.cameraview.frame.Frame;
import com.otaliastudios.cameraview.frame.FrameProcessor;

import java.util.List;

/**
 * The CameraActivity makes access to the phone camera and provides a qr-code scanner.
 */
public class CameraActivity extends AppCompatActivity {
    // The TAG is there to tag the activity.
    private static final String TAG = "CameraActivity";

    // The CameraView shows the view of the camera.
    CameraView mCameraView;

    // This boolean is true if a barcode is detected.
    boolean mIsDetected = false;

    // This is the button to start a scan.
    ImageView mStart;

    // In these options are different types of qr-codes and needed to detect the right one.
    FirebaseVisionBarcodeDetectorOptions mOptions;

    // This is the detector to gain access to the qr-codes.
    FirebaseVisionBarcodeDetector mDetector;

    /**
     * Here the layout objects are initialized and permissions are shown.
     *
     * @param _savedInstanceState saves the current state of the activity
     */
    @Override
    protected void onCreate(Bundle _savedInstanceState) {
        super.onCreate(_savedInstanceState);
        setContentView(R.layout.activity_camera);

        mStart = findViewById(R.id.activity_camera_icon);
        mStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "CameraActivity: onClick() Button");
                Intent i = new Intent(CameraActivity.this, BeratungActivity.class);
                startActivity(i);
            }
        });

        ImageView backButton = findViewById(R.id.activity_cameran_back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "MainActivity: onClick() Button");
                Intent i;
                String extra = getIntent().getStringExtra("FROM_ACTIVITY");
                if (extra.equals("BERATUNG")) {
                    i = new Intent(CameraActivity.this, BeratungActivity.class);
                } else {
                    i = new Intent(CameraActivity.this, MainActivity.class);
                }
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });

        Dexter.withActivity(this).withPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO})
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        setupCamera();
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

                    }
                }).check();

        //Securtity Exception

    }

    /**
     * This method sets the camera up with the help of firebase ml kit.
     */
    private void setupCamera() {
        /*mStart = findViewById(R.id.activity_camera_icon);
        mStart.setEnabled(mIsDetected);
        mStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                mIsDetected = !mIsDetected;
                mStart.setEnabled(mIsDetected);
            }
        });

         */

        mCameraView = findViewById(R.id.activity_camera_cameraview);
        mCameraView.setLifecycleOwner(this);
        mCameraView.addFrameProcessor(new FrameProcessor() {
            @Override
            public void process(@NonNull Frame _frame) {
                processImage(getVisionImageFromFrame(_frame));
            }
        });

        mOptions = new FirebaseVisionBarcodeDetectorOptions.Builder()
                .setBarcodeFormats(FirebaseVisionBarcode.FORMAT_QR_CODE)
                .build();
        mDetector = FirebaseVision.getInstance().getVisionBarcodeDetector(mOptions);
    }

    /**
     * Here is checked if the code can be detected. If not, a toast is shown.
     *
     * @param _image is the image of the code
     */
    private void processImage(FirebaseVisionImage _image) {
        if (!mIsDetected) {
            mDetector.detectInImage(_image)
                    .addOnSuccessListener(new OnSuccessListener<List<FirebaseVisionBarcode>>() {
                        @Override
                        public void onSuccess(List<FirebaseVisionBarcode> _firebaseVisionBarcodes) {
                            if (!mIsDetected)
                                processResults(_firebaseVisionBarcodes);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception _e) {
                    Toast.makeText(CameraActivity.this, "" + _e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    /**
     * In this method the results of the code are getting detected.
     *
     * @param _firebaseVisionBarcodes consist of code types
     */
    private void processResults(List<FirebaseVisionBarcode> _firebaseVisionBarcodes) {
        if (_firebaseVisionBarcodes.size() > 0) {
            mIsDetected = true;
            mStart.setEnabled(true);
            int valType = _firebaseVisionBarcodes.get(0).getValueType();

            if (valType == FirebaseVisionBarcode.TYPE_TEXT) {
                Log.i(TAG, "BeratungActivity: scanned qr code");
                Intent i = new Intent(CameraActivity.this, BeratungActivity.class)
                        .putExtra("FROM_ACTIVITY", "BERATUNG");
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            } else
                createDialog("This qr code isn't working!");
        }
    }


    /**
     * Provides the vision image of the code.
     *
     * @param _frame consists of all the data about the code
     * @return the data of the image as a FirebaseVisionImage
     */
    private FirebaseVisionImage getVisionImageFromFrame(Frame _frame) {
        byte[] data = _frame.getData();
        FirebaseVisionImageMetadata metadata = new FirebaseVisionImageMetadata.Builder()
                .setFormat(FirebaseVisionImageMetadata.IMAGE_FORMAT_NV21)
                .setHeight(_frame.getSize().getHeight())
                .setWidth(_frame.getSize().getWidth())
                .build();
        return FirebaseVisionImage.fromByteArray(data, metadata);
    }

    /**
     * This method creates a dialog with the given text.
     *
     * @param _text is shown in the dialog
     */
    private void createDialog(String _text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(_text).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface _dialog, int _which) {
                _dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
