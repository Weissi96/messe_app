package at.fhooe.mc.sempro2020;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

/**
 * This class provides the fragments which are listed in the timeline. These fragments show the
 * most important information of a consultation.
 */
public class MainFragment extends Fragment {
     // The TAG is there to tag the fragment.
    private static final String TAG = "MainFragment";


     // This value is an id of each campus from 1 to 4. 1 is "Hagenberg" and the others are
     // alphabetically ordered. 0 is the "FH OÖ".
    int mCampus;

    /**
     * The constructor provides the campus for the fragment.
     *
     * @param _campus
     */
    public MainFragment(int _campus) {
        mCampus = _campus;
    }

    /**
     * The constructor which initializes the fragment.
     *
     * @return the initialized frament
     */
    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment(0);
        return fragment;
    }

    /**
     * This method initializes the fragment.
     *
     * @param _savedInstanceState saves the current state of the activity
     */
    @Override
    public void onCreate(@Nullable Bundle _savedInstanceState) {
        super.onCreate(_savedInstanceState);
    }

    /**
     * Here the view and the whole layout objects are getting initialized.
     *
     * @param _inflater           initializes the layout file into the view
     * @param _container          where the view is inside
     * @param _savedInstanceState saves the current state of the activity
     * @return the view
     */
    @Override
    public View onCreateView(LayoutInflater _inflater, ViewGroup _container,
                             Bundle _savedInstanceState) {
        final View view = _inflater.inflate(R.layout.fragment_main, _container, false);
        TextView headline = view.findViewById(R.id.fm_main_headline);
        TextView text = view.findViewById(R.id.fm_main_textfield);
        TextView start = view.findViewById(R.id.fm_main_start);
        TextView end = view.findViewById(R.id.fm_main_end);
        int color;

        switch (mCampus) {
            case 1:
                color = getResources().getColor(R.color.hagenbergGruen);
                headline.setText("Beratung 1");
                setStars(view, 3);
                setAngebot(view, true);
                text.setText("Mobile Computing\n" + "Automotive Computing\n" + "One day at FH");
                start.setText("14:15");
                end.setText("14:37");
                break;
            case 2:
                color = getResources().getColor(R.color.linzGruen);
                headline.setText("Beratung 2");
                setStars(view, 5);
                setAngebot(view, false);
                text.setText("Soziale Arbeit\n" + "Medizintechnik");
                start.setText("14:42");
                end.setText("14:59");
                break;
            case 3:
                color = getResources().getColor(R.color.steyrGelb);
                headline.setText("Beratung 3");
                setStars(view, 3);
                setAngebot(view, true);
                text.setText("Global Sales\n" + "Shuttle");
                start.setText("15:07");
                end.setText("15:31");
                break;
            case 4:
                color = getResources().getColor(R.color.welsBlau);
                headline.setText("Beratung 4");
                setStars(view, 4);
                setAngebot(view, false);
                text.setText("Mechatronik/Wirtschaft\n");
                start.setText("15:43");
                end.setText("16:11");
                break;
            default:
                color = getResources().getColor(R.color.fhooeRot);
        }

        ImageView fmBackground = view.findViewById(R.id.fm_main_background_colored);
        fmBackground.setColorFilter(color, PorterDuff.Mode.SRC_OVER);

        ImageView fmBackgroundArrow = view.findViewById(R.id.fm_main_arrow);
        fmBackgroundArrow.setColorFilter(color);

        ImageView star = view.findViewById(R.id.fm_main_star);
        star.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "BewertungActivity: onClick() Button");
                Intent i = new Intent(getContext(), BewertungActivity.class)
                        .putExtra("FROM_ACTIVITY", "MAIN")
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });

        star = view.findViewById(R.id.fm_main_star);
        star.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "BewertungActivity: onClick() Button");
                Intent i = new Intent(getContext(), BewertungActivity.class)
                        .putExtra("FROM_ACTIVITY", "MAIN")
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });

        star = view.findViewById(R.id.fm_main_star1);
        star.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "BewertungActivity: onClick() Button");
                Intent i = new Intent(getContext(), BewertungActivity.class)
                        .putExtra("FROM_ACTIVITY", "MAIN")
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });

        star = view.findViewById(R.id.fm_main_star2);
        star.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "BewertungActivity: onClick() Button");
                Intent i = new Intent(getContext(), BewertungActivity.class)
                        .putExtra("FROM_ACTIVITY", "MAIN")
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });

        star = view.findViewById(R.id.fm_main_star3);
        star.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "BewertungActivity: onClick() Button");
                Intent i = new Intent(getContext(), BewertungActivity.class)
                        .putExtra("FROM_ACTIVITY", "MAIN")
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });

        star = view.findViewById(R.id.fm_main_star4);
        star.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "BewertungActivity: onClick() Button");
                Intent i = new Intent(getContext(), BewertungActivity.class)
                        .putExtra("FROM_ACTIVITY", "MAIN")
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });


        return view;
    }

    /**
     * This method sets the stars based on the rating the user gave.
     *
     * @param _view is the given view
     * @param _num  is the rating
     */
    void setStars(View _view, int _num) {
        ImageView star = _view.findViewById(R.id.fm_main_star4_filled);
        if (_num > 0)
            star.setVisibility(_view.VISIBLE);

        star = _view.findViewById(R.id.fm_main_star3_filled);
        if (_num > 1)
            star.setVisibility(_view.VISIBLE);

        star = _view.findViewById(R.id.fm_main_star2_filled);
        if (_num > 2)
            star.setVisibility(_view.VISIBLE);

        star = _view.findViewById(R.id.fm_main_star1_filled);
        if (_num > 3)
            star.setVisibility(_view.VISIBLE);

        star = _view.findViewById(R.id.fm_main_star_filled);
        if (_num > 4)
            star.setVisibility(_view.VISIBLE);
    }

    /**
     * This method shows the filled circle if an offer was taken into account.
     *
     * @param _view    is the given view
     * @param isFilled is true when an offer was taken
     */
    void setAngebot(View _view, boolean isFilled) {
        ImageView angebot = _view.findViewById(R.id.fm_main_circle_filled);
        if (isFilled) {
            angebot.setVisibility(_view.VISIBLE);
        } else {
            angebot.setVisibility(_view.INVISIBLE);
        }
    }
}
