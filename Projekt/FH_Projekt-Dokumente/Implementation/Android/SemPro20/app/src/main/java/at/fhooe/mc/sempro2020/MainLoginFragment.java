package at.fhooe.mc.sempro2020;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

/**
 * This class provides the fragment of the login which is shown in the timeline.
 */
public class MainLoginFragment extends Fragment {
    // The TAG is there to tag the fragment.
    private static final String TAG = "MainLoginFragment";

    // This value is the start time.
    public static long mTime;

    // If the start time has already been chosen, this value is true.
    public boolean mTimeCheck = false;

    // This ImageView is shown when the login is finished.
    ImageView mCheckLogin;

    // This is the TextView where the end time is shown.
    TextView mTimeLoginEnd;

    // This is the TextView where the start time is shown.
    TextView mTimeLoginStart;

    /**
     * Default Constructor.
     */
    public MainLoginFragment() {
    }

    /**
     * The constructor which initializes the fragment.
     *
     * @return the initialized frament
     */
    public static MainLoginFragment newInstance() {
        MainLoginFragment fragment = new MainLoginFragment();
        return fragment;
    }


    /**
     * This method initializes the fragment.
     *
     * @param _savedInstanceState saves the current state of the activity
     */
    @Override
    public void onCreate(@Nullable Bundle _savedInstanceState) {
        super.onCreate(_savedInstanceState);
    }

    /**
     * Here the view and the whole layout objects are getting initialized.
     *
     * @param _inflater           initializes the layout file into the view
     * @param _container          where the view is inside
     * @param _savedInstanceState saves the current state of the activity
     * @return the view
     */
    @Override
    public View onCreateView(LayoutInflater _inflater, ViewGroup _container,
                             Bundle _savedInstanceState) {
        final View view = _inflater.inflate(R.layout.fragment_main_login, _container, false);

        mTimeLoginEnd = view.findViewById(R.id.fm_main_login_end);
        mTimeLoginStart = view.findViewById(R.id.fm_main_login_start);
        mCheckLogin = view.findViewById(R.id.activity_main_login_simple_check);

        ImageView login = view.findViewById(R.id.fm_main_login_background);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                mTime = System.currentTimeMillis();
                Log.i(TAG, "CameraActivity: onClick() Button");
                Intent i = new Intent(getContext(), LoginActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });

        login = view.findViewById(R.id.fm_main_login_background_colored);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                mTime = System.currentTimeMillis();
                Log.i(TAG, "CameraActivity: onClick() Button");
                Intent i = new Intent(getContext(), LoginActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });

        if (LoginActivity.mRightCode && !mTimeCheck) {
            mTimeCheck = true;
            setCheckVisible();
            setTime();
        }
        return view;
    }

    /**
     * Sets the visibility of the check icon.
     */
    public void setCheckVisible() {
        mCheckLogin.setVisibility(View.VISIBLE);
    }

    /**
     * Sets the time when the login has finished.
     */
    public void setTime() {
        long endTime = System.currentTimeMillis();
        mTimeLoginEnd.setText(String.format("%1$TH:%1$TM", endTime));
        mTimeLoginStart.setText(String.format("%1$TH:%1$TM", mTime));
    }
}
