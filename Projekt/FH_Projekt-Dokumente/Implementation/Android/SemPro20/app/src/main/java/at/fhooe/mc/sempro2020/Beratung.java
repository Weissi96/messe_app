package at.fhooe.mc.sempro2020;

import java.util.List;

/**
 * A Beratungs-Object describes a Session-List with a start- and end-time and with a
 * BeraterId and a VeranstaltungsId.
 */
public class Beratung {

    // The VeranstaltungsId is an integer value which stays the same for a whole event.
    private int VeranstaltungsId;

    // The BeraterId is an id which belongs to a single consulting.
    private int BeraterId;

    // Sessions is a list of many Sessions.
    private List<Session> sessions;

    /**
     * This Beratung-constructer initializes all given member variables.
     *
     * @param _veranstltungId a given id for a single event
     * @param _beraterId      a given id for a single consulting
     * @param _sessions       a list of sessions
     */
    public Beratung(int _veranstltungId, int _beraterId, List<Session> _sessions) {
        VeranstaltungsId = _veranstltungId;
        BeraterId = _beraterId;
        sessions = _sessions;
    }

    /**
     * This is the default constructor.
     */
    public Beratung() {
        VeranstaltungsId = -1;
        BeraterId = -1;
        sessions = null;
    }
}
