package at.fhooe.mc.sempro2020;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * The EditActivity provides fragments of the important information about the university
 * and the person which is interviewed.
 */
public class EditActivity extends AppCompatActivity {
    // The TAG is there to tag the activity.
    private static final String TAG = "EditActivity";

    // Is true if the Campus fragment is activated.
    boolean mCampus = true;

    // Is true if the Bachelor fragment is activated.
    boolean mBachelor = true;

    // Is true if the Master fragment is activated.
    boolean mMaster = true;

    // Is true if the Angebot fragment is activated.
    boolean mAngebot = true;

    // Is true if the fragment is activated.
    boolean mInteressen = true;

    // Is a EditCampusFragment.
    EditCampusFragment mFmCampus = new EditCampusFragment();

    // Is a EditAngebotFragment.
    EditAngebotFragment mFmAngebot = new EditAngebotFragment();

    // Is a EditBachelorFragment.
    EditBachelorFragment mFMBachelor = new EditBachelorFragment();

    // Is a EditInteressenFragment.
    EditInteressenFragment mFmInteressen = new EditInteressenFragment();


    /**
     * In this method the fragments are activated or not within initialized buttons.
     *
     * @param _savedInstanceState saves the current state of the activity
     */
    @Override
    protected void onCreate(Bundle _savedInstanceState) {
        super.onCreate(_savedInstanceState);
        setContentView(R.layout.activity_edit);

        activateFragment(mFmCampus);
        activateFragment(mFMBachelor);
        activateFragment(mFmAngebot);
        activateFragment(mFmInteressen);

        ImageView check = findViewById(R.id.activity_edit_check);
        check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "BeratungActivity: onClick() check-Button");
                Intent i = new Intent(EditActivity.this, BeratungActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });

        final Button campus = findViewById(R.id.activity_edit_campus);
        campus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "BeratungActivity: onClick() campus-Button");
                if (!mCampus) {
                    campus.setBackgroundColor(getResources().getColor(R.color.fhooeRot));
                    mCampus = true;
                    mFmCampus.getView().setVisibility(View.VISIBLE);
                } else {
                    campus.setBackgroundColor(getResources().getColor(R.color.fhooeRotTransparenz));
                    mCampus = false;
                    mFmCampus.getView().setVisibility(View.GONE);
                }
            }
        });

        final Button angebot = findViewById(R.id.activity_edit_angebot);
        angebot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "BeratungActivity: onClick() angebot-Button");
                if (!mAngebot) {
                    angebot.setBackgroundColor(getResources().getColor(R.color.fhooeRot));
                    mAngebot = true;
                    mFmAngebot.getView().setVisibility(View.VISIBLE);
                } else {
                    angebot.setBackgroundColor(getResources().getColor(R.color.fhooeRotTransparenz));
                    mAngebot = false;
                    mFmAngebot.getView().setVisibility(View.GONE);
                }
            }
        });

        final Button interessen = findViewById(R.id.activity_edit_interessen);
        interessen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "BeratungActivity: onClick() interessen-Button");
                if (!mInteressen) {
                    interessen.setBackgroundColor(getResources().getColor(R.color.fhooeRot));
                    mInteressen = true;
                    mFmInteressen.getView().setVisibility(View.VISIBLE);
                } else {
                    interessen.setBackgroundColor(getResources().getColor(R.color.fhooeRotTransparenz));
                    mInteressen = false;
                    mFmInteressen.getView().setVisibility(View.GONE);
                }
            }
        });

        final Button bachelor = findViewById(R.id.activity_edit_bachelor);
        bachelor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "BeratungActivity: onClick() bachelor-Button");
                if (!mBachelor) {
                    bachelor.setBackgroundColor(getResources().getColor(R.color.fhooeRot));
                    mBachelor = true;
                    mFMBachelor.getView().setVisibility(View.VISIBLE);
                } else {
                    bachelor.setBackgroundColor(getResources().getColor(R.color.fhooeRotTransparenz));
                    mBachelor = false;
                    mFMBachelor.getView().setVisibility(View.GONE);
                }
            }
        });

        final Button master = findViewById(R.id.activity_edit_master);
        master.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "BeratungActivity: onClick() master-Button");
                if (!mMaster) {
                    master.setBackgroundColor(getResources().getColor(R.color.fhooeRot));
                    mMaster = true;
                    Toast toast = Toast.makeText(EditActivity.this, "Master Button clicked!", Toast.LENGTH_SHORT);
                    toast.show();
                } else {
                    master.setBackgroundColor(getResources().getColor(R.color.fhooeRotTransparenz));
                    mMaster = false;
                }
            }
        });
    }

    /**
     * This method activates the fragment.
     *
     * @param _fm is the given fragment
     */
    private void activateFragment(Fragment _fm) {
        FragmentManager mgr = getSupportFragmentManager();
        FragmentTransaction ft = mgr.beginTransaction();
        ft.add(R.id.activity_edit_linearlayout, _fm);
        ft.commit();
    }
}
