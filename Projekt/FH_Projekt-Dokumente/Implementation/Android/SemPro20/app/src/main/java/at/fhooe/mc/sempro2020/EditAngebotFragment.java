package at.fhooe.mc.sempro2020;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


/**
 * This fragment shows the offers.
 */
public class EditAngebotFragment extends Fragment {
    // The TAG is there to tag the fragment.
    private static final String TAG = "EditAngebotFragment";

    // Is true if it is selected.
    boolean mCircleOneDay = false;

    // Is true if it is selected.
    boolean mCircleShuttle = false;

    // Is true if it is selected.
    boolean mCircleBeratung = false;

    // Is the key to save the right value to the shared preferences.
    public static String ONEDAY_KEY = String.valueOf(R.string.angebot_oneday_key);

    // Is the key to save the right value to the shared preferences.
    public static String SHUTTLE_KEY = String.valueOf(R.string.angebot_shuttle_key);

    // Is the key to save the right value to the shared preferences.
    public static String BERATUNG_KEY = String.valueOf(R.string.angebot_beratung_key);

    // The shared preferences are used to save the data locally.
    SharedPreferences sp;

    /**
     * Default constructor.
     */
    public EditAngebotFragment() {
    }

    /**
     * The constructor which inititializes the fragment.
     *
     * @return the initialized frament
     */
    public static EditAngebotFragment newInstance() {
        EditAngebotFragment fragment = new EditAngebotFragment();
        return fragment;
    }

    /**
     * This method initializes the fragment.
     *
     * @param savedInstanceState saves the current state of the activity
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Here the view and the whole layout objects are getting initialized.
     *
     * @param _inflater           initializes the layout file into the view
     * @param _container          where the view is inside
     * @param _savedInstanceState saves the current state of the activity
     * @return the view
     */
    @Override
    public View onCreateView(LayoutInflater _inflater, ViewGroup _container,
                             Bundle _savedInstanceState) {
        final View view = _inflater.inflate(R.layout.fragment_edit_angebot, _container, false);

        sp = BeratungAdapter.mContext.getSharedPreferences(BeratungAdapter.PREF_KEY, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sp.edit();
        editor.clear().apply();

        final ImageView onedayCircle = view.findViewById(R.id.fm_edit_angebot_oneday_circle);
        onedayCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "EditAngebotFragment: onClick() oneDay-checkbox");
                ImageView circleNew = view.findViewById(R.id.fm_edit_angebot_oneday_circle_filled);
                if (!mCircleOneDay) {
                    circleNew.setVisibility(View.VISIBLE);
                    editor.putString(ONEDAY_KEY, "One Day at FH");
                    editor.apply();
                    mCircleOneDay = true;
                } else {
                    circleNew.setVisibility(View.INVISIBLE);
                    editor.remove(ONEDAY_KEY);
                    editor.apply();
                    mCircleOneDay = false;
                }
            }
        });

        final ImageView shuttleCircle = view.findViewById(R.id.fm_edit_angebot_shuttle_circle);
        shuttleCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "EditAngebotFragment: onClick() shuttle-checkbox");
                ImageView circleNew = view.findViewById(R.id.fm_edit_angebot_shuttle_circle_filled);
                if (!mCircleShuttle) {
                    circleNew.setVisibility(View.VISIBLE);
                    editor.putString(SHUTTLE_KEY, "Shuttledienst");
                    editor.apply();
                    mCircleShuttle = true;
                } else {
                    circleNew.setVisibility(View.INVISIBLE);
                    editor.remove(SHUTTLE_KEY);
                    editor.apply();
                    mCircleShuttle = false;
                }
            }
        });

        final ImageView beratungCircle = view.findViewById(R.id.fm_edit_angebot_beratung_circle);
        beratungCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "EditAngebotFragment: onClick() beratung-checkbox");
                ImageView circleNew = view.findViewById(R.id.fm_edit_angebot_beratung_circle_filled);
                if (!mCircleBeratung) {
                    circleNew.setVisibility(View.VISIBLE);
                    editor.putString(BERATUNG_KEY, "Persönliche Beratung");
                    editor.apply();
                    mCircleBeratung = true;
                } else {
                    circleNew.setVisibility(View.INVISIBLE);
                    editor.remove(BERATUNG_KEY);
                    editor.apply();
                    mCircleBeratung = false;
                }
            }
        });

        return view;
    }
}
