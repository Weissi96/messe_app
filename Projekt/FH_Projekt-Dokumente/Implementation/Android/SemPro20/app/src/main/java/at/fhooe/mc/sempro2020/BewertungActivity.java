package at.fhooe.mc.sempro2020;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

/**
 * This activity displays the feedback option for a consultation.
 */
public class BewertungActivity extends AppCompatActivity {

    // The TAG is there to tag the activity.
    private static final String TAG = "BewertungActivity";

    // This boolean shows if the circle of "one day at fh" is filled or not.
    private boolean mIsOneDay = false;

    // This boolean shows if the circle of "Shuttledienst" is filled or not.
    private boolean mIsShuttle = false;

    // This boolean shows if the circle of "persönliche Beratung" is filled or not.
    private boolean mIsBeratung = false;

    /**
     * The layout objects are getting initialized.
     *
     * @param _savedInstanceState saves the current state of the activity
     */
    @Override
    protected void onCreate(Bundle _savedInstanceState) {
        super.onCreate(_savedInstanceState);
        setContentView(R.layout.activity_bewertung);


        Button checkButton = findViewById(R.id.activity_bewertung_button);

        checkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "MainActivity: onClick() Button");
                Intent i = new Intent(BewertungActivity.this, MainActivity.class);
                startActivity(i);
            }
        });


        ImageView backButton = findViewById(R.id.activity_bewertung_back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "MainActivity: onClick() Button");
                Intent i;
                String extra = getIntent().getStringExtra("FROM_ACTIVITY");
                if (extra.equals("BERATUNG")) {
                    i = new Intent(BewertungActivity.this, BeratungActivity.class);
                } else {
                    i = new Intent(BewertungActivity.this, MainActivity.class);
                }
                startActivity(i);
            }
        });


        final ImageView oneDayCircle = findViewById(R.id.activity_bewertung_circle);
        oneDayCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                ImageView cirleNew = findViewById(R.id.activity_bewertung_circle_filled);
                if (!mIsOneDay) {
                    cirleNew.setVisibility(_v.VISIBLE);
                    mIsOneDay = true;
                } else {
                    cirleNew.setVisibility(_v.INVISIBLE);
                    mIsOneDay = false;
                }
            }
        });

        final ImageView shuttleCircle = findViewById(R.id.activity_bewertung_circle1);
        shuttleCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                ImageView cirleNew = findViewById(R.id.activity_bewertung_circle1_filled);
                if (!mIsShuttle) {
                    cirleNew.setVisibility(_v.VISIBLE);
                    mIsShuttle = true;
                } else {
                    cirleNew.setVisibility(_v.INVISIBLE);
                    mIsShuttle = false;
                }
            }
        });

        final ImageView beratungCircle = findViewById(R.id.activity_bewertung_circle2);
        beratungCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                ImageView cirleNew = findViewById(R.id.activity_bewertung_circle2_filled);
                if (!mIsBeratung) {
                    cirleNew.setVisibility(_v.VISIBLE);
                    mIsBeratung = true;
                } else {
                    cirleNew.setVisibility(_v.INVISIBLE);
                    mIsBeratung = false;
                }
            }
        });
    }
}
