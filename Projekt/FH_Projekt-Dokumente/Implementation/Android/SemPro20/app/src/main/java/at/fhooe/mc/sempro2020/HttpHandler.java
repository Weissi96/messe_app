package at.fhooe.mc.sempro2020;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class HttpHandler {

    private static final String TAG = "HttpHandler";

    public HttpHandler() {
    }

    public String makeServiceCall(String _url) {
        String response = null;
        try {
            URL url = new URL(_url);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);
        } catch (MalformedURLException _e) {
            Log.e(TAG, "MalformedURLException: " + _e.getMessage());
        } catch (ProtocolException _e) {
            Log.e(TAG, "ProtocolException: " + _e.getMessage());
        } catch (IOException _e) {
            Log.e(TAG, "IOException: " + _e.getMessage());
        } catch (Exception _e) {
            Log.e(TAG, "Exception: " + _e.getMessage());
        }
        return response;
    }

    private String convertStreamToString(InputStream _is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(_is));
        StringBuilder sb = new StringBuilder();

        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException _e) {
            _e.printStackTrace();
        } finally {
            try {
                _is.close();
            } catch (IOException _e) {
                _e.printStackTrace();
            }
        }

        return sb.toString();
    }
}
