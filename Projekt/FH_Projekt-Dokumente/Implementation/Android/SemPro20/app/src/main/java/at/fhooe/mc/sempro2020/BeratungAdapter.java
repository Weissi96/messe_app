package at.fhooe.mc.sempro2020;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

/**
 * The BeratungsAdapter is an adapter of the recycler view and handles the fragments. It also
 * extends of a viewholder.
 */
public class BeratungAdapter extends RecyclerView.Adapter<BeratungAdapter.BeratungViewHolder> {

    // The TAG is there to tag the adapter.
    private static final String TAG = "BeratungAdapter";

    // This list consists of the String-definition of the single consultations.
    public static List<String> mBeratungList;

    // The context of the class.
    public static Context mContext;

    // The PREF_KEY is defined to gain access to all variables in the shared preferences.
    public static String PREF_KEY = String.valueOf(R.string.beratung_text_key);

    /**
     * This is the constructor of the BeratungAdpater and initializes the member variable.
     *
     * @param _context is the context of the adapter
     */
    public BeratungAdapter(Context _context) {
        mContext = _context;
    }

    /**
     * This is the constructor of the BeratungAdpater and initializes the member variable.
     *
     * @param _list    is the list of consultations
     * @param _context is the context of the adapter
     */
    public BeratungAdapter(List<String> _list, Context _context) {
        mBeratungList = _list;
        mContext = _context;
    }

    /**
     * This method creates the viewHolder which is needed to show the fragments.
     *
     * @param _parent   is the parent view
     * @param _viewType is the type of the view
     * @return a BeratungViewHolder
     */
    @NonNull
    @Override
    public BeratungViewHolder onCreateViewHolder(@NonNull ViewGroup _parent, int _viewType) {
        Log.i(TAG, "onCreateViewHolder");

        LayoutInflater inflater = LayoutInflater.from(_parent.getContext());
        View view = inflater.inflate(R.layout.fragment_beratung, _parent, false);
        BeratungViewHolder viewHolder = new BeratungViewHolder(view);

        return viewHolder;
    }

    /**
     * In this method the components of the viewHolder are getting initialized.
     *
     * @param _holder   is a BeratungsViewHolder
     * @param _position is the position the list object needs to fit
     */
    @Override
    public void onBindViewHolder(@NonNull BeratungViewHolder _holder, int _position) {
        _holder.mHeadline.setText(mBeratungList.get(_position));
    }

    /**
     * Get the size of the list.
     *
     * @return the size of the list
     */
    @Override
    public int getItemCount() {
        return mBeratungList.size();
    }

    /**
     * The BeratungViewHolder is a ViewHolder and a nested class of the BeratungAdapter.
     */
    public class BeratungViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "BeratungViewHolder";
        SharedPreferences sp;
        String defVal = "";
        public TextView mHeadline;
        public TextView mText;

        public BeratungViewHolder(@NonNull final View _itemView) {
            super(_itemView);

            mHeadline = _itemView.findViewById(R.id.fm_beratung_headline);
            mText = _itemView.findViewById(R.id.fm_beratung_text);

            ImageView person = _itemView.findViewById(R.id.fm_beratung_person);
            person.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View _v) {
                    Log.i(TAG, "PersonActivity: onClick() Button");
                    Intent i = new Intent(_v.getContext(), PersonActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    _v.getContext().startActivity(i);
                }
            });

            ImageView edit = _itemView.findViewById(R.id.fm_beratung_edit);
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View _v) {
                    Log.i(TAG, "EditActivity: onClick() Button");
                    Intent i = new Intent(_v.getContext(), EditActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    _v.getContext().startActivity(i);
                }
            });

            ImageView copy = _itemView.findViewById(R.id.fm_beratung_copy);
            copy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View _v) {
                    Log.i(TAG, "BeratungActivity: onClick() Button");
                    Toast toast = Toast.makeText(_v.getContext(), "Copy Beratung!", Toast.LENGTH_SHORT);
                    toast.show();
                    BeratungAdapter.mBeratungList.add("Beratung 2");
                }
            });

            _itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return false;
                }
            });

            sp = mContext.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE);
            setPersonText(sp);
        }

        public void setPersonText(SharedPreferences _sp) {
            String personText = "";
            String hgb = _sp.getString(EditCampusFragment.HGB_KEY, defVal);
            String lnz = _sp.getString(EditCampusFragment.LNZ_KEY, defVal);
            String str = _sp.getString(EditCampusFragment.STR_KEY, defVal);
            String wls = _sp.getString(EditCampusFragment.WLS_KEY, defVal);

            String oneDay = _sp.getString(EditAngebotFragment.ONEDAY_KEY, defVal);
            String shuttle = _sp.getString(EditAngebotFragment.SHUTTLE_KEY, defVal);
            String beratung = _sp.getString(EditAngebotFragment.BERATUNG_KEY, defVal);

            String eStudien = _sp.getString(EditInteressenFragment.ESTUDIEN_KEY, defVal);
            String info = _sp.getString(EditInteressenFragment.INFORMATIK_KEY, defVal);
            String life = _sp.getString(EditInteressenFragment.LIFESCIENCES_KEY, defVal);
            String medien = _sp.getString(EditInteressenFragment.MEDIENKOMM_KEY, defVal);
            String soziales = _sp.getString(EditInteressenFragment.SOZIALES_KEY, defVal);
            String technik = _sp.getString(EditInteressenFragment.TECHNIK_KEY, defVal);
            String umwelt = _sp.getString(EditInteressenFragment.UMWELT_KEY, defVal);
            String wManage = _sp.getString(EditInteressenFragment.WIRTMANAGE_KEY, defVal);
            String wTechnik = _sp.getString(EditInteressenFragment.WIRTTECH_KEY, defVal);

            //campus

            if (hgb.equals("Hagenberg")) {
                personText += hgb + "\n";
            }
            if (lnz.equals("Linz")) {
                personText += lnz + "\n";
            }
            if (str.equals("Steyr")) {
                personText += str + "\n";
            }
            if (wls.equals("Wels")) {
                personText += wls + "\n";
            }

            //Angebote

            if (oneDay.equals("One Day at FH")) {
                personText += oneDay + "\n";
            }
            if (shuttle.equals("Shuttledienst")) {
                personText += shuttle + "\n";
            }
            if (beratung.equals("Persönliche Beratung")) {
                personText += beratung + "\n";
            }

            //Interessen

            if (eStudien.equals("Englischsprachige Studien")) {
                personText += eStudien + "\n";
            }
            if (info.equals("Informatik")) {
                personText += info + "\n";
            }
            if (life.equals("Life Sciences")) {
                personText += life + "\n";
            }
            if (medien.equals("Medien & Kommunikation")) {
                personText += medien + "\n";
            }
            if (soziales.equals("Soziales")) {
                personText += soziales + "\n";
            }
            if (technik.equals("Technik")) {
                personText += technik + "\n";
            }
            if (umwelt.equals("Umwelt & Energie")) {
                personText += umwelt + "\n";
            }
            if (wManage.equals("Wirtschaft & Management")) {
                personText += wManage + "\n";
            }
            if (wTechnik.equals("Wirtschaft & Technik")) {
                personText += wTechnik + "\n";
            }

            if (!personText.equals(""))
                mText.setText(personText);
        }
    }
}
