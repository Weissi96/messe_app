package at.fhooe.mc.sempro2020;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator;

/**
 * The BeratungActivity describes the consultations and lists them. The deleting and finishing
 * of a consulting happens here. Also a consulting can be added or copied.
 */
public class BeratungActivity extends AppCompatActivity {

    // The TAG is there to tag an activity.
    private static final String TAG = "BeratungActivity";

    // The shared preferences are used to save the data locally.
    SharedPreferences sp;

    // The default value defVal is a string to get access to a certain saved value in the shared preferences.
    String defVal = "";

    // This value describes a person which takes part in a consulting.
    Person mPerson;

    // A session consists of all important information about the university the person is interested in.
    Session mSession;

    // A Beratung has at least one session.
    Beratung mBeratung;

    // The RecyclerView is used to list the consultations.
    RecyclerView mRecyclerView;

    // The BeratungAdapter is used for the Beratung-fragments in the recyclerView.
    BeratungAdapter mAdapter;

    // This is a list of consultations which are shown in the view.
    List<String> mBeratungList;

    // This ItemTouchHelper is needed for the deleting and finishing within a swipe gesture.
    ItemTouchHelper.SimpleCallback mSimpleCallback;

    // This String is used if a consultation is deleted but will be undone again.
    String mDeletedBeratung = null;

    // This boolean is used to check if the time has already been started.
    boolean mIsFirstStart = true;

    // This long value consists of the start time of the consultation.
    long startTime;

    //
    long remainingTime;

    /**
     * In the onCreate method the mentioned variables are initialized. The values are saved
     * in the shared preferences and the swipe gestures are implemented. The whole layout
     * is generated.
     *
     * @param _savedInstanceState saves the current state of the activity
     */
    @Override
    protected void onCreate(Bundle _savedInstanceState) {
        super.onCreate(_savedInstanceState);
        setContentView(R.layout.activity_beratung);

        sp = getSharedPreferences(getString(R.string.beratung_text_key), Context.MODE_PRIVATE);

        mPerson = new Person();
        mPerson.setGeschlecht(sp.getString(PersonActivity.GESCHLECHT_KEY, defVal));
        mPerson.setBundesland(sp.getString(PersonActivity.BUNDESLAND_KEY, defVal));
        mPerson.setSchule(sp.getString(PersonActivity.SCHULE_KEY, defVal));
        mPerson.setSchultyp(sp.getString(PersonActivity.SCHULTYP_KEY, defVal));

        mSession = new Session();
        mBeratung = new Beratung();

        startTime = System.currentTimeMillis() + 3600000;
        remainingTime = System.currentTimeMillis() - startTime;

        //handler


        //ImageView addPerson = findViewById(R.id.activity_beratung_add_person);
       /* addPerson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _v) {
                Log.i(TAG, "CameraActivity: onClick() Button");
                Intent i = new Intent(BeratungActivity.this, CameraActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });*/

        mBeratungList = new ArrayList<>();
        mBeratungList.add("Beratung 1");

        mRecyclerView = findViewById(R.id.activity_beratung_recyclerview);
        mAdapter = new BeratungAdapter(mBeratungList, this);
        mRecyclerView.setAdapter(mAdapter);

        mSimpleCallback = new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView _recyclerView,
                                  @NonNull RecyclerView.ViewHolder _viewHolder,
                                  @NonNull RecyclerView.ViewHolder _target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder _viewHolder, int _direction) {
                final int pos = _viewHolder.getAdapterPosition();
                switch (_direction) {
                    case ItemTouchHelper.LEFT:
                        mDeletedBeratung = mBeratungList.get(pos);
                        mBeratungList.remove(pos);
                        mAdapter.notifyItemRemoved(pos);
                        Snackbar.make(mRecyclerView, mDeletedBeratung, Snackbar.LENGTH_LONG)
                                .setAction("Undo", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View _v) {
                                        mBeratungList.add(pos, mDeletedBeratung);
                                        mAdapter.notifyItemInserted(pos);
                                    }
                                }).show();
                        break;
                    case ItemTouchHelper.RIGHT:
                        createDialog("Beratung speichern & bewerten?", "save");
                        mAdapter.notifyItemChanged(pos);

                        Gson gson = new Gson();
                        String json = gson.toJson(mBeratung);

                        //TODO: service call needs to be here!!

                        break;
                }
            }


            @Override
            public void onChildDraw(@NonNull Canvas _c, @NonNull RecyclerView _recyclerView, @NonNull RecyclerView.ViewHolder _viewHolder, float _dX, float _dY, int _actionState, boolean _isCurrentlyActive) {

                new RecyclerViewSwipeDecorator.Builder(_c, _recyclerView, _viewHolder, _dX, _dY, _actionState, _isCurrentlyActive)
                        .addSwipeLeftBackgroundColor(ContextCompat.getColor(BeratungActivity.this, R.color.fhooeRot))
                        .addSwipeLeftActionIcon(R.drawable.icon_delete)
                        .addSwipeRightBackgroundColor(ContextCompat.getColor(BeratungActivity.this, R.color.hagenbergGruen))
                        .addSwipeRightActionIcon(R.drawable.icon_check)
                        .create()
                        .decorate();


                super.onChildDraw(_c, _recyclerView, _viewHolder, _dX, _dY, _actionState, _isCurrentlyActive);
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(mSimpleCallback);
        itemTouchHelper.attachToRecyclerView(mRecyclerView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        timeHandler();
    }

    @Override
    protected void onPause() {
        super.onPause();
        timeHandler();
    }

    private void timeHandler() {
        final Handler someHandler = new Handler(getMainLooper());
        someHandler.postDelayed(new Runnable() {
            TextView time = findViewById(R.id.activity_beratung_time);

            @Override
            public void run() {
                time.setText(String.format("%1$TH:%1$TM:%1$TS", remainingTime));
                someHandler.postDelayed(this, 1000);
                remainingTime = System.currentTimeMillis() - startTime;
            }
        }, 10);
    }

    /**
     * When a user presses back on the native smartphone back-button a dialog is created.
     */
    @Override
    public void onBackPressed() {
        createDialog("Beratung abbrechen?", "backButton");
    }

    /**
     * This method creates a dialog with a String based text and a content-String which
     * helps defining the dialog type.
     *
     * @param _text    is a String which is shown in the dialog
     * @param _content is a String and describes the type of dialog
     */
    private void createDialog(String _text, final String _content) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(_text).setPositiveButton("JA", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface _dialog, int _which) {
                _dialog.dismiss();
                Intent i;
                switch (_content) {
                    case "backButton":
                        Log.i(TAG, "MainActivity: onClick() Button");
                        i = new Intent(BeratungActivity.this, MainActivity.class)
                                .putExtra("FROM_ACTIVITY", "MAIN")
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        break;
                    case "save":
                        Log.i(TAG, "BewertungActivity: onClick() Button");
                        i = new Intent(BeratungActivity.this, BewertungActivity.class)
                                .putExtra("FROM_ACTIVITY", "BERATUNG")
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        break;
                    case "lastItem":
                        break;
                    default:
                        break;
                }
            }
        }).setNegativeButton("NEIN", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface _dialog, int _which) {
                _dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * @param _view
     */
    public void addPerson(View _view) {
        Log.i(TAG, "CameraActivity: onClick() Button");
        Intent i = new Intent(BeratungActivity.this, CameraActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }
}
