package at.fhooe.mc.sempro2020;

/**
 * A person object consists of a "Geschlecht", a "Schultyp", a "Bundesland" and a "Schule".
 * This should describe the person which is interviewed.
 */
public class Person {
    // This String is the sex of the person.
    String Geschlecht;

    // This String describes the type of school the person attends to.
    String Schultyp;

    // This String describes where the person is from.
    String Bundesland;

    // This String is the school the person attends to.
    String Schule;

    /**
     * This constructor initializes all parameters.
     *
     * @param _geschlecht is the sex
     * @param _schultyp   is the type of school
     * @param _bundesland is the province/state
     * @param _schule     is the school
     */
    public Person(String _geschlecht, String _schultyp, String _bundesland, String _schule) {
        Geschlecht = _geschlecht;
        Schultyp = _schultyp;
        Bundesland = _bundesland;
        Schule = _schule;
    }

    /**
     * Default constructor.
     */
    public Person() {
        Geschlecht = "Geschlecht";
        Schultyp = "Schultyp";
        Bundesland = "Bundesland";
        Schule = "Schule(spezifisch)";
    }

    /**
     * Sets the Geschlecht of the person.
     *
     * @param _geschlecht is the sex
     */
    public void setGeschlecht(String _geschlecht) {
        Geschlecht = _geschlecht;
    }

    /**
     * Sets the Geschlecht of the person.
     *
     * @param _schultyp is the type of school
     */
    public void setSchultyp(String _schultyp) {
        Schultyp = _schultyp;
    }

    /**
     * Sets the Geschlecht of the person.
     *
     * @param _bundesland is the province/state
     */
    public void setBundesland(String _bundesland) {
        Bundesland = _bundesland;
    }

    /**
     * Sets the Geschlecht of the person.
     *
     * @param _schule is the school
     */
    public void setSchule(String _schule) {
        Schule = _schule;
    }
}
