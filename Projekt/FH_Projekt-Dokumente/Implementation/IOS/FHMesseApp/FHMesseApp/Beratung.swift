//
//  Beratung.swift
//  FHMesseApp
//
//  Created by Theresa Ganser on 03.02.20.
//  Copyright © 2020 Theresa Ganser. All rights reserved.
//

/// struct for serialization
struct Beratung: Codable {
    let BeraterId: Int
    let VeranstaltungId: Int
    //let Sessions = [Session]()
}
