//
//  SessionItemCell.swift
//  FHMesseApp
//
//  Created by Theresa Ganser on 25.01.20.
//  Copyright © 2020 Theresa Ganser. All rights reserved.
//

import UIKit

/// the labels and views of the Superview are connected with the class
class SessionItemCell:UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
}
