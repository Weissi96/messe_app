//
//  Person.swift
//  FHMesseApp
//
//  Created by Theresa Ganser on 03.02.20.
//  Copyright © 2020 Theresa Ganser. All rights reserved.
//

/// struct for serialization
struct Person: Codable {
    let gender: String
    let schooltype: String
    let state: String
    let school: String
}
