//
//  LoginViewController.swift
//  FHMesseApp
//
//  Created by Theresa Ganser on 20.01.20.
//  Copyright © 2020 Theresa Ganser. All rights reserved.
//

import UIKit

/// View Controller of the Login Page
class LoginViewController: UIViewController {
    
    /// Text Field in which enter the code to login
    @IBOutlet weak var loginTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    /// go back to the previous view
    /// - Parameter sender: button which is clicked to call this method
    @IBAction func returnToStart(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    /// checks if the login code is valid, if the code is valid the user
    /// go to the next view othewise an alert is printed
    /// - Parameter sender: button which is clicked to call this method
    @IBAction func enterLogin(_ sender: UIButton) {
        if loginTextField.text == "123" {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Overview", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "timeline") as! OverviewTableViewController
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
        else {
            let alert = UIAlertController(title: "Invalid code", message: "Try again!", preferredStyle: .alert)
            self.present(alert, animated: true)
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                alert.dismiss(animated: true)
            }
        }
    }
}
