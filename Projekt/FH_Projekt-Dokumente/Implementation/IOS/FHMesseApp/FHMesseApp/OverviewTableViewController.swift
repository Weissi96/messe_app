//
//  OverviewTableViewController.swift
//  FHMesseApp
//
//  Created by Theresa Ganser on 21.01.20.
//  Copyright © 2020 Theresa Ganser. All rights reserved.
//

import UIKit

/// view controller which contains a table view with multiplte consultions items
class OverviewTableViewController: UIViewController {
    
    /// connection to the tableView of the ViewController
    @IBOutlet weak var overviewTableView: UITableView!
    
    var consultations: [Consultation] = [
        Consultation(title: "Beratung 1", details: "MC\nmännlich\nHTL Neufelden"),
        Consultation(title: "Beratung 2", details: "AMTM\nweiblich\nHAK Rohrbach"),
        Consultation(title: "Beratung 3", details: "CRF\nweiblich\nHAK Rohrbach"),
        Consultation(title: "Beratung 4", details: "AC\nmännlich\nHTL Linz"),
        Consultation(title: "Beratung 5", details: "HSD\nAUT\nmännlich\nHTL Linz")
    ]

    override func viewDidLoad() {
         super.viewDidLoad()
     }

    /// calls the super method of it, reload the data and change the backgroundView
     override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        overviewTableView.delegate = self
        overviewTableView.dataSource = self
        overviewTableView.reloadData()
        overviewTableView.backgroundView = HorizontalView(frame: self.view.bounds)
    }
    
    /// inner class which sets the background to specific format and colors
    class HorizontalView: UIView {
        /// draw two rectangles with 2 different colors
        /// - Parameter rect: a rectangle object which is given by call the method
        override func draw(_ rect: CGRect) {
            super.draw(rect)
            let redPart = CGRect(x: 0, y: 0, width: rect.size.width/3, height: rect.size.height)
            UIColor.init(red: 206.0/255, green: 68.0/255, blue: 83.0/255, alpha: 1.0).set()
            guard let contextRed = UIGraphicsGetCurrentContext() else { return }
            contextRed.fill(redPart)

            let greyPart = CGRect(x: rect.size.width/3, y: 0, width: 2*rect.size.width/3, height: rect.size.height)
            UIColor.init(red: 220.0/255, green: 220.0/255, blue: 220.0/255, alpha: 1.0).set()
            guard let contextGray = UIGraphicsGetCurrentContext() else { return }
            contextGray.fill(greyPart)
        }
    }
}

/// create a extension to call the tableView methods because they tableView is inside a view controller
extension OverviewTableViewController: UITableViewDelegate, UITableViewDataSource {
    
    /// get the automatic dimension ot the table view
    /// - Parameters:
    ///     - tableView: table view of the view controller
    ///     - heightForRowAt:passes index path of height for a row
    /// - Returns: a CGFloat object of table view
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    /// counts the consultation objects
    /// - Parameters:
    ///     - tableView: table view of the view controller
    ///     - numberOfRowsInSection: passes the number of rows in a section
    /// - Returns: the number of consultations
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return consultations.count
    }
    
    /// Set the text of the consultation object to the labels in the view
    /// - Parameters:
    ///     - tableView: table view of the view controller
    ///     - cellForRowAt: passes index path of cell for row
    /// - Returns: a table cell with the values which are set
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell", for: indexPath) as! ConsultationCell
        let item = consultations[indexPath.row]
        cell.titleLabel?.text = item.title
        cell.detailsLabel?.text = item.details
        
        let allStudies = item.details.split(separator: "\n")
        let studies = allStudies[0]
        
        switch studies {
            case "MC","AC":
                cell.colorView.backgroundColor = UIColor.init(red: 135.0/255, green: 185.0/255, blue: 25.0/255, alpha: 1.0)
                cell.triangleView.tintColor = UIColor.init(red: 135.0/255, green: 185.0/255, blue: 25.0/255, alpha: 1.0)
        case "AMTM":
                cell.colorView.backgroundColor = UIColor.init(red: 0.0/255, green: 120.0/255, blue: 190.0/255, alpha: 1.0)
                cell.triangleView.tintColor = UIColor.init(red: 0.0/255, green: 120.0/255, blue: 190.0/255, alpha: 1.0)
        case "CRF":
            cell.colorView.backgroundColor = UIColor.init(red: 225.0/255, green: 170.0/255, blue: 0.0/255, alpha: 1.0)
            cell.triangleView.tintColor = UIColor.init(red: 225.0/255, green: 170.0/255, blue: 0.0/255, alpha: 1.0)
        default:
            cell.colorView.backgroundColor = UIColor.init(red: 190.0/255, green: 5.0/255, blue: 25.0/255, alpha: 1.0)
            cell.triangleView.tintColor = UIColor.init(red: 190.0/255, green: 5.0/255, blue: 25.0/255, alpha: 1.0)
        }
        return cell
    }
}
