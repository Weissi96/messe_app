//
//  SessionItem.swift
//  FHMesseApp
//
//  Created by Theresa Ganser on 25.01.20.
//  Copyright © 2020 Theresa Ganser. All rights reserved.
//

import UIKit

/// represent a session item with a title and details
class SessionItem {
    var title: String
    var details: String
    
    /// initialize the two variables
    /// - Parameters:
    ///     - title: title of a session item (eg Beratung 1)
    ///     - details: details of a session item (gender, school, interests)
    init(title: String, details: String) {
        self.title = title
        self.details = details
    }
}
