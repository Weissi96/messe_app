//
//  AddDetailsViewController.swift
//  FHMesseApp
//
//  Created by Theresa Ganser on 27.01.20.
//  Copyright © 2020 Theresa Ganser. All rights reserved.
//

import UIKit

/// view controller which contains a stackView and multiple buttons and views
class AddDetailsViewController: UIViewController {
    var red = UIColor.init(red: 206.0/255, green: 68.0/255, blue: 83.0/255, alpha: 1.0)
    var darkred = UIColor.init(red: 190.0/255, green: 5.0/255, blue: 25.0/255, alpha: 1.0)
    
    var campusSelected = true
    var offerSelected = false
    var interestSelected = false
    var bachelorSelected = false
    var masterSelected = false
    
    // connection to the buttons to filter which views are shown
    @IBOutlet weak var campusButton: UIButton!
    @IBOutlet weak var offerButton: UIButton!
    @IBOutlet weak var interestButton: UIButton!
    @IBOutlet weak var bachelorButton: UIButton!
    @IBOutlet weak var masterButton: UIButton!

    @IBOutlet weak var scroller: UIScrollView!
    @IBOutlet weak var checkView: UIView!
    @IBOutlet weak var stackView: UIStackView!
    
    // views in the stack view
    @IBOutlet weak var campusView: UIView!
    @IBOutlet weak var offerView: UIView!
    @IBOutlet weak var interestView: UIView!
    @IBOutlet weak var HGBView: UIView!
    @IBOutlet weak var LinzView: UIView!
    @IBOutlet weak var SteyrView: UIView!
    @IBOutlet weak var WelsView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scroller.translatesAutoresizingMaskIntoConstraints = false
        stackView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(scroller)
        view.addSubview(checkView)
        scroller.addSubview(stackView)

        offerView.isHidden = true
        interestView.isHidden = true
        HGBView.isHidden = true
        LinzView.isHidden = true
        SteyrView.isHidden = true
        WelsView.isHidden = true
        
        offerButton.backgroundColor = red
        interestButton.backgroundColor = red
        bachelorButton.backgroundColor = red
        masterButton.backgroundColor = red
    }
    
     /// calls the super method of it and set the content size of the scroll view
    override func viewWillLayoutSubviews(){
        super.viewWillLayoutSubviews()
        //scroller.contentSize = CGSize(width: scroller.contentSize.width, height: 3200)
        scroller.contentSize = CGSize(width: scroller.contentSize.width, height: scroller.contentSize.height+200)
    }
    
    /// send data which is selected by the view above
    /// - Parameter sender: button which is clicked to call this method
    @IBAction func sendData(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

// FilterButtons
    /// if button is clicked the specific view is shown
    /// - Parameter sender: button which is clicked to call this method
    @IBAction func clickFilterButton(_ sender: UIButton) {
        if sender.backgroundColor!.isEqual(red) {
            sender.backgroundColor = darkred
        }
        else {
            sender.backgroundColor = red
        }
        
        switch sender {
            case campusButton:
                campusSelected = !campusSelected
                campusView.isHidden = !campusView.isHidden
                scroller.focusItems(in: campusView.frame)
                //scroller.contentOffset = CGPoint(x:0, y: campusView.)
                
                
                if bachelorSelected == true {
                    hgbButton.setImage(UIImage(systemName: "circle.fill"), for: .normal)
                    linzButton.setImage(UIImage(systemName: "circle.fill"), for: .normal)
                    steyrButton.setImage(UIImage(systemName: "circle.fill"), for: .normal)
                    welsButton.setImage(UIImage(systemName: "circle.fill"), for: .normal)

                    HGBView.isHidden = false
                    LinzView.isHidden = false
                    SteyrView.isHidden = false
                    WelsView.isHidden = false
                }
                else if bachelorSelected == false  {
                    hgbButton.setImage(UIImage(systemName: "circle"), for: .normal)
                    linzButton.setImage(UIImage(systemName: "circle"), for: .normal)
                    steyrButton.setImage(UIImage(systemName: "circle"), for: .normal)
                    welsButton.setImage(UIImage(systemName: "circle"), for: .normal)
                    HGBView.isHidden = true
                    LinzView.isHidden = true
                    SteyrView.isHidden = true
                    WelsView.isHidden = true
            }
                // hide all filled Buttons (all of HGB, Linz, Steyr, Wels)
                
            case offerButton:
                offerSelected = !offerSelected
                offerView.isHidden = !offerView.isHidden
            
            case interestButton:
                interestSelected = !interestSelected
                interestView.isHidden = !interestView.isHidden
                interestView.updateFocusIfNeeded()
           
            case bachelorButton:
                bachelorSelected = !bachelorSelected
                if bachelorSelected == true {
                    if campusSelected == false {
                        HGBView.isHidden = false
                        LinzView.isHidden = false
                        SteyrView.isHidden = false
                        WelsView.isHidden = false
                    }
                    else {
                        if hgbButton.currentImage!.isEqual(UIImage(systemName: "circle.fill")) {
                            HGBView.isHidden = false
                        }
                        if linzButton.currentImage!.isEqual(UIImage(systemName: "circle.fill")) {
                            LinzView.isHidden = false
                        }
                        if steyrButton.currentImage!.isEqual(UIImage(systemName: "circle.fill")) {
                            SteyrView.isHidden = false
                        }
                        if welsButton.currentImage!.isEqual(UIImage(systemName: "circle.fill")) {
                            WelsView.isHidden = false
                        }
                    }
                }
                // else hide all views and filled Buttons?? (all of HGB, Linz, Steyr, Wels)
                
            case masterButton:
                masterSelected = !masterSelected
            default:
                return
        }
        stackView.layoutIfNeeded()
        scroller.layoutIfNeeded()
    }

    
// Enter CampusButtons
    // connection to the buttons of the campus
    @IBOutlet weak var hgbButton: UIButton!
    @IBOutlet weak var linzButton: UIButton!
    @IBOutlet weak var steyrButton: UIButton!
    @IBOutlet weak var welsButton: UIButton!
    
    /// if button is clicked the button is selected/unselected
    /// - Parameter sender: button which is clicked to call this method
    @IBAction func selectCampus(_ sender: UIButton) {
        if sender.currentImage!.isEqual(UIImage(systemName: "circle.fill")) {
            sender.setImage(UIImage(systemName: "circle"), for: .normal)
        }
        else {
            sender.setImage(UIImage(systemName: "circle.fill"), for: .normal)
        }
        
        switch sender {
            case hgbButton:
                HGBView.isHidden = !HGBView.isHidden
            case linzButton:
                LinzView.isHidden = !LinzView.isHidden
            case steyrButton:
                SteyrView.isHidden = !SteyrView.isHidden
            case welsButton:
                WelsView.isHidden = !WelsView.isHidden
            default:
                return
        }
        stackView.layoutIfNeeded()
        scroller.layoutIfNeeded()
    }
    
    
// enter OfferButtons
    // connection to the buttons of the offers
    @IBOutlet weak var dayFHButton: UIButton!
    @IBOutlet weak var beratungButton: UIButton!
    @IBOutlet weak var shuttleButton: UIButton!
    
    /// if button is clicked the button is selected/unselected
    /// - Parameter sender: button which is clicked to call this method
    @IBAction func selectOffers(_ sender: UIButton) {
        if sender.currentImage!.isEqual(UIImage(systemName: "circle.fill")) {
            sender.setImage(UIImage(systemName: "circle"), for: .normal)
        }
        else {
            sender.setImage(UIImage(systemName: "circle.fill"), for: .normal)
        }
        // für speichern was ausgewählt dann switch mit sender ist Angebot ...
    }

    
// enter InterestButtons
    // connection to the buttons of the interests
    @IBOutlet weak var englischButton: UIButton!
    @IBOutlet weak var informatikButton: UIButton!
    @IBOutlet weak var lifeScButton: UIButton!
    @IBOutlet weak var medienButton: UIButton!
    @IBOutlet weak var sozialesButton: UIButton!
    @IBOutlet weak var technikButton: UIButton!
    @IBOutlet weak var umweltButton: UIButton!
    @IBOutlet weak var WirtschaftManagButton: UIButton!
    @IBOutlet weak var WirtschaftTechnButton: UIButton!
    
    /// if button is clicked the button is selected/unselected
    /// - Parameter sender: button which is clicked to call this method
    @IBAction func selectInterests(_ sender: UIButton) {
        if sender.currentImage!.isEqual(UIImage(systemName: "circle.fill")) {
            sender.setImage(UIImage(systemName: "circle"), for: .normal)
        }
        else {
            sender.setImage(UIImage(systemName: "circle.fill"), for: .normal)
        }
        // für speichern was ausgewählt dann switch mit sender ist Angebot ...
        // 2 Variablen erstellen die speichern ob image circle fill oder circle / ob button selected oder unselected
    }
  
    
// HGBView
    // connection to the buttons of HagenbergView
    @IBOutlet weak var ACButton: UIButton!
    @IBOutlet weak var HSDButton: UIButton!
    @IBOutlet weak var KWMButton: UIButton!
    @IBOutlet weak var MDTButton: UIButton!
    @IBOutlet weak var MBIButton: UIButton!
    @IBOutlet weak var MCButton: UIButton!
    @IBOutlet weak var SIBButton: UIButton!
    @IBOutlet weak var SEButton: UIButton!
    
    /// if button is clicked the button is selected/unselected
    /// - Parameter sender: button which is clicked to call this method
    @IBAction func selectHGBCourses(_ sender: UIButton) {
        if sender.currentImage!.isEqual(UIImage(systemName: "circle.fill")) {
            sender.setImage(UIImage(systemName: "circle"), for: .normal)
        }
        else {
            sender.setImage(UIImage(systemName: "circle.fill"), for: .normal)
        }
    }
    
// LinzView
    // connection to the buttons of LinzView
    @IBOutlet weak var medtechButton: UIButton!
    @IBOutlet weak var sozialVerwButton: UIButton!
    @IBOutlet weak var sozialeArbButton: UIButton!
    
    /// if button is clicked the button is selected/unselected
    /// - Parameter sender: button which is clicked to call this method
    @IBAction func selectLinzCourses(_ sender: UIButton) {
        if sender.currentImage!.isEqual(UIImage(systemName: "circle.fill")) {
            sender.setImage(UIImage(systemName: "circle"), for: .normal)
        }
        else {
            sender.setImage(UIImage(systemName: "circle.fill"), for: .normal)
        }
    }
// SteyrView
    // connection to the buttons of SteyrView
    @IBOutlet weak var rwButton: UIButton!
    @IBOutlet weak var globalSalesButton: UIButton!
    @IBOutlet weak var logistikButton: UIButton!
    @IBOutlet weak var marketingButton: UIButton!
    @IBOutlet weak var produktionButton: UIButton!
    @IBOutlet weak var prozessmanagButton: UIButton!
    
    /// if button is clicked the button is selected/unselected
    /// - Parameter sender: button which is clicked to call this method
    @IBAction func selectSteyrCourses(_ sender: UIButton) {
        if sender.currentImage!.isEqual(UIImage(systemName: "circle.fill")) {
            sender.setImage(UIImage(systemName: "circle"), for: .normal)
        }
        else {
            sender.setImage(UIImage(systemName: "circle.fill"), for: .normal)
        }
    }
//WelsView
    // connection to the buttons of WelsView
    @IBOutlet weak var agrarButton: UIButton!
    @IBOutlet weak var energieTechButton: UIButton!
    @IBOutlet weak var autButton: UIButton!
    @IBOutlet weak var bauingButton: UIButton!
    @IBOutlet weak var bioButton: UIButton!
    @IBOutlet weak var electButton: UIButton!
    @IBOutlet weak var maschBauButton: UIButton!
    @IBOutlet weak var entwMetallButton: UIButton!
    @IBOutlet weak var innovmanaButton: UIButton!
    @IBOutlet weak var prodtechButton: UIButton!
    @IBOutlet weak var lebensmittlTechButton: UIButton!
    @IBOutlet weak var leichtbauButton: UIButton!
    @IBOutlet weak var mechatronikButton: UIButton!
    @IBOutlet weak var proddesignButton: UIButton!
    @IBOutlet weak var verfahTechProd: UIButton!
    
    /// if button is clicked the button is selected/unselected
    /// - Parameter sender: button which is clicked to call this method
    @IBAction func selectWelsCourses(_ sender: UIButton) {
        if sender.currentImage!.isEqual(UIImage(systemName: "circle.fill")) {
            sender.setImage(UIImage(systemName: "circle"), for: .normal)
        }
        else {
            sender.setImage(UIImage(systemName: "circle.fill"), for: .normal)
        }
    }
}
