//
//  PersonViewController.swift
//  FHMesseApp
//
//  Created by Theresa Ganser on 25.01.20.
//  Copyright © 2020 Theresa Ganser. All rights reserved.
//

import UIKit

/// view controller which contains multiple pickers and textfields to get information about a person
class PersonViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    /// connection to the text field and the picker (gender part)
    @IBOutlet weak var genderTextField: UITextField!
    @IBOutlet weak var genderPicker: UIPickerView!
    
    /// connection to the text field and the picker (schooltype part)
    @IBOutlet weak var schoolTypText: UITextField!
    @IBOutlet weak var schoolTypPicker: UIPickerView!
    
    /// connection to the text field and the picker (state part)
    @IBOutlet weak var countryText: UITextField!
    @IBOutlet weak var countryPicker: UIPickerView!
    
    /// connection to the text field and the picker (school part)
    @IBOutlet weak var schoolText: UITextField!
    @IBOutlet weak var schoolPicker: UIPickerView!
    
    /// go back to the previous view
    /// - Parameter sender: button which is clicked to call this method
    @IBAction func returnToSession(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    /// connection to the scrollview
    @IBOutlet weak var scrollView: UIScrollView!
    
    var gender = ["Geschlecht", "weiblich", "männlich", "divers"]
    var schoolTypes = ["Schultyp", "HTL", "HAK", "AHS", "BORG", "HBLA", "HBLFA", "BAKIP", "Lehre", "Universität", "Fachhochschule"]
    var countries = ["Bundesland", "Vorarlberg", "Tirol", "Salzburg", "Steiermark", "Kärnten", "Oberösterreich", "Niederösterreich", "Wien", "Burgenland"]
    var schools = ["Schule (spezifisch)", "HTL Neufelden", "HTL Linz", "HAK Rohrbach", "AHS Linz", "BORG Linz", "HBLA Lentia ", "HBLFA Schönbrunn", "BAKIP Linz"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        genderPicker.isHidden = true
        schoolTypPicker.isHidden = true
        countryPicker.isHidden = true
        schoolPicker.isHidden = true
        view.addSubview(scrollView)

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)

        // when user touch around, keyboard is hidden
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    /// calls the super method of it and set the content size of the scroll view
    override func viewWillLayoutSubviews(){
        super.viewWillLayoutSubviews()
        scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: 900)
    }
    
    /// Scrolls up the view, so the user can see the Texfield in which he enter
    /// - Parameter notification: passes a NSNotification
    @objc func keyboardWillShow(notification:NSNotification){
        guard let keyboardFrameValue = notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue else {
            return
        }
        let keyboardFrame = view.convert(keyboardFrameValue.cgRectValue, from: nil)
        scrollView.contentOffset = CGPoint(x:0, y:keyboardFrame.size.height)
    }

    /// hide the keyboard
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }

    /// call the super method of it
    override func didReceiveMemoryWarning() {
           super.didReceiveMemoryWarning()
    }
    
    /// Set number of components
    /// - Parameter pickerView: passes a picker view
    /// - Returns: number of components
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    /// return the number of values of a picker
    /// - Parameters:
    ///     - pickerView: passes a picker view
    ///     - component: number of rows in component
    /// - Returns: number of values in the picker view
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        switch pickerView.tag {
            case 1:
                return gender.count
            case 2:
                return schoolTypes.count
            case 3:
                return countries.count
            case 4:
                return schools.count
            default:
                return -1
        }
    }

    /// Return the value of the selected row of the picker
    /// - Parameters:
    ///     - pickerView: passes a picker view
    ///     - row: title for row
    ///     - component: number of rows in component
    /// - Returns: value of the specific row
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        self.view.endEditing(true)
        switch pickerView.tag {
            case 1:
                return gender[row]
            case 2:
                return schoolTypes[row]
            case 3:
                return countries[row]
            case 4:
                return schools[row]
            default:
                return nil
        }
    }

    /// Set the selected value of the picker to the text field
    /// - Parameters:
    ///     - pickerView: passes a picker view
    ///     - row: selected row
    ///     - component: component of the picker
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView.tag {
            case 1:
                self.genderTextField.text = self.gender[row]
                self.genderPicker.isHidden = true
            case 2:
                self.schoolTypText.text = self.schoolTypes[row]
                self.schoolTypPicker.isHidden = true
            case 3:
                self.countryText.text = self.countries[row]
                self.countryPicker.isHidden = true
            case 4:
                self.schoolText.text = self.schools[row]
                self.schoolPicker.isHidden = true
            default:
                break
        }
    }

    /// Shows the picker when the user click on the belonging text field
    /// - Parameter textField: textfield which is selected
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.genderTextField {
            self.genderPicker.isHidden = false
        }
        if textField == self.schoolTypText {
            self.schoolTypPicker.isHidden = false
        }
        if textField == self.countryText {
            self.countryPicker.isHidden = false
        }
        if textField == self.schoolText {
            self.schoolPicker.isHidden = false
        }
        //the user should not see the keyboard
        textField.endEditing(true)
    }
    
    /// send data which is selected by the above picker views
    /// - Parameter sender: button which is clicked to call this method
    @IBAction func sendPersonInfo(_ sender: UIButton) {
        func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            // This method is triggered whenever the user makes a change to the picker selection.
            // The parameter named row and component represents what was selected.
        }
        dismiss(animated: true, completion: nil)
    }
}
