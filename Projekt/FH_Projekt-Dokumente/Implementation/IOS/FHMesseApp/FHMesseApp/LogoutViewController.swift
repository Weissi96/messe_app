//
//  LogoutViewController.swift
//  FHMesseApp
//
//  Created by Theresa Ganser on 31.01.20.
//  Copyright © 2020 Theresa Ganser. All rights reserved.
//

import UIKit

/// view controller which contains slider and text fields to get feedback of a fair
class LogoutViewController: UIViewController {
    
    /// connection to the scroll view
    @IBOutlet weak var scroller: UIScrollView!
    
    /// go back to the previous view
    /// - Parameter sender: button which is clicked to call this method
    @IBAction func returnToPrevView(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(scroller)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    /// calls the super method of it and set the content size of the scroll view
    override func viewWillLayoutSubviews(){
        super.viewWillLayoutSubviews()
        scroller.contentSize = CGSize(width: scroller.contentSize.width, height: 900)
    }

    /// Scrolls up the view, so the user can see the Texfield in which he enter
    /// - Parameter notification: passes a NSNotification
    @objc func keyboardWillShow(notification:NSNotification){
        guard let keyboardFrameValue = notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue else {
            return
        }
        let keyboardFrame = view.convert(keyboardFrameValue.cgRectValue, from: nil)
        scroller.contentOffset = CGPoint(x:0, y:keyboardFrame.size.height)
    }

    /// hide the keyboard
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    /// send the feedback to the memory and logout the user
    /// - Parameter sender: button which is clicked to call this method
    @IBAction func sendFeedback(_ sender: UIButton) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "home") as! ViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
}
