//
//  ConsultationCell.swift
//  FHMesseApp
//
//  Created by Theresa Ganser on 23.01.20.
//  Copyright © 2020 Theresa Ganser. All rights reserved.
//

import UIKit

/// the labels and views of the Superview are connected with the class
class ConsultationCell:UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var triangleView: UIImageView!
}
