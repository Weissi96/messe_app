//
//  SessionOverviewTableViewController.swift
//  FHMesseApp
//
//  Created by Theresa Ganser on 25.01.20.
//  Copyright © 2020 Theresa Ganser. All rights reserved.
//

import Foundation
import UIKit
import CoreData

/// view controller which contains a table view with multiplte consultions items
class SessionOverviewTableViewController: UIViewController {
    
    /// connection to the tableView of the ViewController
    @IBOutlet weak var personTableView: UITableView!
    
    /// connection to the timelabel of the ViewController
    @IBOutlet weak var labelTime: UILabel!
    
    var sessionItems: [SessionItem] = [
        SessionItem(title: "Beratung 1", details: "AMTM\nweiblich\nHAK Rohrbach"),
    ]
    
   override func viewDidLoad() {
        super.viewDidLoad()
    }

    /// calls the super method of it, reload the data of the tableView
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        personTableView.delegate = self
        personTableView.dataSource = self
        personTableView.reloadData()
    }
}

/// create a extension to call the tableView methods because they tableView is inside a view controller
extension SessionOverviewTableViewController: UITableViewDataSource, UITableViewDelegate {
    
    /// get the automatic dimension ot the table view
    /// - Parameters:
    ///     - tableView: table view of the view controller
    ///     - heightForRowAt:passes index path of height for a row
    /// - Returns: a CGFloat object of table view
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    /// counts the session item objects
    /// - Parameters:
    ///     - tableView: table view of the view controller
    ///     - numberOfRowsInSection: passes the number of rows in a section
    /// - Returns: the number of session items
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sessionItems.count
    }

    /// Set the text of the session item object to the labels in the view
    /// - Parameters:
    ///     - tableView: table view of the view controller
    ///     - cellForRowAt: passes index path of cell for row
    /// - Returns: a table cell with the values which are set
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sessionCell", for: indexPath) as! SessionItemCell
        let item = sessionItems[indexPath.row]
        cell.titleLabel?.text = item.title
        cell.detailsLabel?.text = item.details
        return cell
    }

    /// create a swipe mechanism to finish or delete a session item and generate additionally a alert controller
    /// - Parameters:
    ///     - tableView: table view of the view controller
    ///     - trailingSwipeActionsConfigurationForRowAt: passes index path of trailing swipe
    ///     action configuration for a row
    /// - Returns: UISwipeActionsConfiguration with the two mechanism finish and delete
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let finishAction = UIContextualAction(style: .normal, title: nil) { action, view, complete in
          complete(true)
            let finishMenu = UIAlertController(title: nil, message: "Beratung abschließen", preferredStyle: .actionSheet)
            let confirmAction = UIAlertAction(title: "Ja", style: .default, handler: { (action) -> Void in
               let storyBoard: UIStoryboard = UIStoryboard(name: "SessionOverview", bundle: nil)
               let vc = storyBoard.instantiateViewController(withIdentifier: "feedback") as! RatingViewController
               vc.modalPresentationStyle = .fullScreen
               self.present(vc, animated: true, completion: nil)
                
                // Serialization
                /*let beratung: [Beratung] = [
                    Beratung(BeraterId: 1, VeranstaltungId: 1)
                ]
                let encoder = JSONEncoder()
                let data = try! encoder.encode(beratung) // Do not use try! in production code
                let string = String(data: data, encoding: .utf8)
                print(string)*/
            })

            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            finishMenu.addAction(confirmAction)
            finishMenu.addAction(cancelAction)
            self.present(finishMenu, animated: true, completion: nil)
        }
        
        finishAction.image = UIImage(systemName: "checkmark.circle.fill")
        finishAction.backgroundColor = UIColor.init(red: 135.0/255, green: 185.0/255, blue: 25.0/255, alpha: 1.0)
        let deleteAction = UIContextualAction(style: .normal, title: nil) { action, view, complete in
          complete(true)
            let deleteMenu = UIAlertController(title: nil, message: "Beratung löschen", preferredStyle: .actionSheet)
            let confirmAction = UIAlertAction(title: "Delete", style: .default, handler: { (action) -> Void in
                self.sessionItems.remove(at: indexPath.row)
                self.personTableView.deleteRows(at: [indexPath], with: .automatic)
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            deleteMenu.addAction(confirmAction)
            deleteMenu.addAction(cancelAction)
            self.present(deleteMenu, animated: true, completion: nil)
        }
        deleteAction.image = UIImage(systemName: "trash")
        deleteAction.backgroundColor = .red
        return UISwipeActionsConfiguration(actions: [finishAction, deleteAction])
    }
}
