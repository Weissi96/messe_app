//
//  Session.swift
//  FHMesseApp
//
//  Created by Theresa Ganser on 03.02.20.
//  Copyright © 2020 Theresa Ganser. All rights reserved.
//

/// struct for serialization
struct Session: Codable {
    let Bewertung: Int
    let Person: Person
    let Angebote = [String]()
    let Campus = [String]()
    let Interessen = [String]()
    let Bachelor = [String]()
    let Master = [String]()
    let start: String
    let end: String
}
