//
//  ViewController.swift
//  FHMesseApp
//
//  Created by Theresa Ganser on 20.01.20.
//  Copyright © 2020 Theresa Ganser. All rights reserved.
//

import UIKit

/// View Controller of the startpage, contains a function to go to next View.
class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    /// Go to the Login Screen
    /// - Parameter sender: button that is clicked to call this method
    @IBAction func userLogin(_ sender: UIButton) {
        performSegue(withIdentifier: "goToLogin", sender: self)
    }
}
