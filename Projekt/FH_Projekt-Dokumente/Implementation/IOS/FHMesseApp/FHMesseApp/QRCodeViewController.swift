//
//  QRCodeViewController.swift
//  FHMesseApp
//
//  Created by Theresa Ganser on 24.01.20.
//  Copyright © 2020 Theresa Ganser. All rights reserved.
//

import UIKit
import AVFoundation

/// View Controller of the QRCodeScanner
class QRCodeViewController: UIViewController {
    /// display if code scan is successful
    @IBOutlet weak var msgLabel: UILabel!
    @IBOutlet weak var returnButton: UIButton!
    
    /// go back to the previous view
    /// - Parameter sender: button which is clicked to call this method
    @IBAction func goBackToOverview(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    var captureSession = AVCaptureSession()
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var qrCodeFrameView:UIView?
    
    /// View is loading and the whole code in this method is executed
    override func viewDidLoad() {
        super.viewDidLoad()
        // Get the back-facing camera
        guard let captureDevice = AVCaptureDevice.default(for: AVMediaType.video) else {
            print("Failed to get the camera device")
            return
        }
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Set the input device on the capture session.
            captureSession.addInput(input)
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession.addOutput(captureMetadataOutput)

            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
            
        } catch {
            print(error)
            return
        }
        
        // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoPreviewLayer?.frame = view.layer.bounds
        view.layer.addSublayer(videoPreviewLayer!)
        
        // Start video capture.
        captureSession.startRunning()
        
        // Move the message label and top bar to the front
        view.bringSubviewToFront(msgLabel)
        view.bringSubviewToFront(returnButton)
        
        // Initialize QR Code Frame to highlight the QR code
        qrCodeFrameView = UIView()
         
        if let qrCodeFrameView = qrCodeFrameView {
            qrCodeFrameView.layer.borderColor = UIColor.black.cgColor
            qrCodeFrameView.layer.borderWidth = 3
            view.addSubview(qrCodeFrameView)
            view.bringSubviewToFront(qrCodeFrameView)
        }
    }

    /// calls the didReceiveMemoryWarning method of the super class
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// generates a alert message, so the user can decide, if he want a new Session entry to the view
    /// - Parameter decodeURL: url from the QRCode
    func launchApp(decodedURL: String) {
        if presentedViewController != nil {
            return
        }
        let alertPrompt = UIAlertController(title: "Add new QRCode", message: "You add a new person.", preferredStyle: .actionSheet)
        let confirmAction = UIAlertAction(title: "Confirm", style: UIAlertAction.Style.default, handler: { (action) -> Void in
//            if let url = URL(string: decodedURL) {
//                if UIApplication.shared.canOpenURL(url) {
//                    UIApplication.shared.open(url)
//                }
//            }
           self.performSegue(withIdentifier: "goToSessionOverview", sender: self)
            
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
        alertPrompt.addAction(confirmAction)
        alertPrompt.addAction(cancelAction)
        present(alertPrompt, animated: true, completion: nil)
    }
}

/// Check if the QR Code is a valid code and generate metadata Objects
extension QRCodeViewController: AVCaptureMetadataOutputObjectsDelegate {
    
    /// Check if the metadataObject is availabel and get it and print out the url of the QRCode
    /// - Parameters:
    ///     - output: metadata output
    ///     - metadataObject: metadata object of QRCode
    ///     - connection: capture connection
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            msgLabel.text = "No QR code detected!"
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if [AVMetadataObject.ObjectType.qr].contains(metadataObj.type) {
            // If the found metadata is equal to the QR code metadata (or barcode) then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds

            if metadataObj.stringValue != nil {
                launchApp(decodedURL: metadataObj.stringValue!)
                msgLabel.text = metadataObj.stringValue
            }
        }
    }
}
